/* 
 * File:   depletion.h
 * Author: dlago_000
 *
 * Created on July 10, 2015, 2:59 PM
 */

#include "tran_mat.h"
#include "matexp.h"
//#include "cram.h"
// #include "stable_solve.h"
#include <cstdlib>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include "math.h"
#include "stdio.h"

using namespace std;

#ifndef DEPLETION_H
#define	DEPLETION_H


/*
 * This class contains the tools and functions to run a burnup calculation 
 * for one time step.
 * 
 */
class depletion {
public:
    // constructor
    depletion();
    // destructor
    virtual ~depletion();
    
    // set instance of library
    void set_library(Library_builder*);

    // function to set initial concentration vector of stable isotopes
 //   void set_initial_conc_stable(vector<double>, tran_mat*);
    
    // function to set initial concentration vector of unstable isotopes
    void set_initial_conc(vector<double>, vector<double>, tran_mat*);
    
    // function to set reaction rates for time step
    void set_reaction_rates(vector< vector<double> >);
    
    void set_time_interval(double);
    
    // for solving the stable isotopes on final time step
    void set_stable_time_interval(double);
    
    void set_final_conc_u(vector<double>);
    
    void set_final_conc_s(vector<double>);
    
    // function to retrieve new concentrations after time step
    void get_new_conc(vector<double>&, tran_mat*);
    
    // get functions 
    double get_time();
    
    // get functions 
    double get_stable_time();
    
    // getters for initial concentrations
    double get_initial_conc_u(int);
    double get_initial_conc_s(int);
    
    // reorder function to put final concentration in same order it was fed in
    void reorder_conc(tran_mat*, bool);
    
   
    /*
     * This function takes in the instance of the transition matrix and 
     * calls a solver specified by the user to find the nuclide concentrations
     * after a specified time interval .
     * Takes in:
     * 1. instance of transition matrix
     * 2. bool stating whether or not reaction rates exist
     * 3. Identifier for which solver to use
     * For now, only matrix exponential method (method = 1)
     * 
     */
    void run(depletion*, tran_mat*, bool, bool, int, int);
    
//    /*
//     * This function solves for the final concentrations of the 
//     * isotopes reduced from the transition matrix. This should only be called
//     * after the time step to find the full final concentration vector
//     * 
//     */
//    void stable_solver(tran_mat*, vector<double>&);
    
    // instance of decay library
    Library_builder* library;
    
protected:
    
    // initial concentrations at beginning of time step
    vector<double> initial_conc_s; // stable
    vector<double> initial_conc_u; // unstable
    
    // final concentrations after time step
    vector<double> final_conc_s;
    vector<double> final_conc_u;
    vector<double> final_conc;
    
    // reaction rates vector for problem
    vector<double> reaction_rates;
    
    // length of time step
    double time_interval;  
    // stable time for final solve
    double stable_time;
    
private:

};

#endif	/* DEPLETION_H */

