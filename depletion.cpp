/* 
 * File:   depletion.cpp
 * Author: dlago_000
 * 
 * Created on July 10, 2015, 2:59 PM
 */

#include "depletion.h"
#include "tran_mat.h"
#include "cram.h"
#include "stable_solve.h"
#include <cstdlib>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include "math.h"
#include "stdio.h"

using namespace std;

// Constructor initializes solver class
depletion::depletion() {
}

void depletion::set_library(Library_builder* lib) {
    // set instance of Library to use with chain solver method
    library = lib;
}

// Destructor clears arrays
depletion::~depletion() {
    initial_conc_s.clear();
    initial_conc_u.clear();
    final_conc.clear();
    reaction_rates.clear();
}

void depletion::set_initial_conc( vector<double> conc_i_unstable, vector<double> conc_i_stable, tran_mat* matrix ){
    
    for(int i=0; i<conc_i_unstable.size(); i++){
        initial_conc_u.push_back(conc_i_unstable[i]);
    }
    for(int i=0; i<conc_i_stable.size(); i++){
        initial_conc_s.push_back(conc_i_stable[i]);
    }
    
}

void depletion::set_reaction_rates( vector< vector <double> > rxn_rates){
//    for( int i=0; i < rxn_rates.size(); i++){
//        reaction_rates.push_back(rxn_rates[i]);
//    }
}

void depletion::set_time_interval( double time){
    time_interval = time;
}

void depletion::set_stable_time_interval( double time){
    stable_time = time;
}

void depletion::set_final_conc_u( vector<double> final_u){
    for( int i=0; i < final_u.size(); i++){
        final_conc_u.push_back(final_u[i]);
    }
    if(final_conc_u.size() != initial_conc_u.size()){
        cout << "WARNING: SIZE OF INITIAL UNSTABLE ISOTOPE CONCENTRATION DIFFERENT\n"
                " FROM FINAL SIZE!!!" << endl;
        cout << "Final size = " << final_u.size() << endl;
        cout << "Initial size = " << initial_conc_u.size() << endl;
        
    }
}

void depletion::set_final_conc_s( vector<double> final_s){
    for( int i=0; i < final_s.size(); i++){
        final_conc_s.push_back(final_s[i]);
    }
    if(final_s.size() != initial_conc_s.size()){
        cout << "WARNING: SIZE OF INITIAL STABLE ISOTOPE CONCENTRATION DIFFERENT\n"
                " FROM FINAL SIZE!!!" << endl;
        cout << "Final size = " << final_s.size() << endl;
        cout << "Initial size = " << initial_conc_s.size() << endl;
    }
}


// returns new concentration in ascending order
void depletion::get_new_conc( vector<double>& conc_f , tran_mat* matrix){
    // call isotope reordering function to return in ascending order
    // with both stable and unstable isotopes
    cout << "BEFORE REORDER " << endl;
    if(initial_conc_s.size() > 0){
        reorder_conc(matrix, true);
    }
    else{
        reorder_conc(matrix, false);
    }
    cout << "AFTER REORDER" << endl;
    for( int i=0; i < final_conc.size(); i++){
        conc_f.push_back(final_conc[i]);
    }
}

double depletion::get_time(){
    return time_interval;
}

double depletion::get_stable_time(){
    return stable_time;
}

double depletion::get_initial_conc_u(int i){
   // cout << "IN GETTER, DOES IT WORK?!?!?!, also size = " << initial_conc_u.size() << endl;
    return initial_conc_u[i];
}

double depletion::get_initial_conc_s(int i){
   // cout << "IN GETTER, DOES IT WORK?!?!?!, also size = " << initial_conc_u.size() << endl;
    return initial_conc_s[i];
}


void depletion::reorder_conc(tran_mat* matrix, bool some_stable){
    // reorder final concentration vector to be output
    // returns in original order fed into depletion 
    int iter1=0, iter2=0;
    bool found = false;
    // cout << "SIZE OF ORIGINAL ORDER " << matrix->nuclides_original_order.size() << endl;
    for(int i=0; i<matrix->nuclides_original_order.size(); i++){
       //  cout << "!!!!! ORIGINAL ORDER TMAT !!!!! = " << matrix->nuclides_original_order[i] << endl;
        for(int j=0; j<matrix->unstable.size(); j++){
            if(matrix->unstable[j] == matrix->nuclides_original_order[i]){
//                cout << "Turning " << j << " INTO " << i << endl;
                final_conc.push_back(final_conc_u[j]);
            }  
        }
        if(some_stable){
            for(int j=0; j<matrix->stable.size(); j++){
                if(matrix->stable[j] == matrix->nuclides_original_order[i]){
                    final_conc.push_back(final_conc_s[j]);
                }
            }
        }
    }
}


void depletion::run(depletion* decay_param, tran_mat* matrix, bool rxns, bool stable_checks, int method, int k){
    
    // call method specified to solve burnup equations
    // 1 == matrix exponential
    // 2 == CRAM
    // more?
    if(method == 1){
    //    mat_exp_solver(matrix, initial_conc_u, final_conc, reaction_rates, time_interval, 1E-8);
    }
    if(method == 2){
        // CRAM method, input order of quadrature coefficients for 
        // exponential approximation
        cram* cram1 = new cram;
        cram1->cram_solver_1(decay_param, matrix, k);
        cram1->~cram();       
    }
    
    //solve for stable isotopes and isotopes reduced from original matrix
    // for now, use empty dummy thing
    vector<double> stables;//(matrix->stable.size(),0.0);
    stables = initial_conc_s;
    
    if(stable_checks){
        
        // After matrix solver, call solver for stable isotopes
    
        // instantiate class for stable solver
        stable_solve* chain = new stable_solve;
 
        if(matrix->reactions){
            // need to add production bateman equation solver
            // for now use regular chain solver
            chain->batch_solve2(decay_param, matrix, stables);
        }
        if(not matrix->reactions){
            chain->batch_solve2(decay_param, matrix, stables);
        }
    
        // clean up instance
        chain->~stable_solve();
        
        //    stable_solver(matrix, stables);
        set_final_conc_s(stables);
    }
    if(not stable_checks){
        //    stable_solver(matrix, stables);
        set_final_conc_s(stables);
    }
    
}

//void depletion::stable_solver(tran_mat* matrix, vector<double>& stables){
//    
//    int index;
//    
//    int stable_count = 0;
//    
//    // col_ind_s points to element in concentration vector of unstable isotopes
//    for(int i=0; i<matrix->stable_value.size(); i++){
//        if(matrix->col_ind_s[i] == matrix->row_ind_s[i]){
//            index = matrix->row_ind_s[i];
//            stable_count++;
//        //    cout << "found stable " << index << endl;
//        }
//        if(matrix->row_ind_s[i] == index and matrix->col_ind_s[i] != index){
//        //    cout << "doing offd stuff " << endl;
//            stables[stable_count-1] += matrix->stable_value[i]*(final_conc_u[matrix->col_ind_s[i]]);
//            cout << stables[stable_count-1] << endl;
//        }
//    }
//    for(int i=0; i<stables.size(); i++){
//    //    cout << "stables[" << i << "] = " << stables[i] << endl;
//    }
//     
//}