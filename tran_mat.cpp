/* 
 * File:   tran_mat.cpp
 * Author: dlago_000
 * 
 * Created on July 10, 2015, 2:47 PM
 */

#include "tran_mat.h"
#include "library_builder.h"
#include <cstdlib>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include "math.h"
#include "stdio.h"

// Constructor initializes transition matrix class
tran_mat::tran_mat() {
}

// Destructor clears arrays
tran_mat::~tran_mat() {
    // clear arrays
    diag.clear();
    off_diag_1.clear();
    col_ind_1.clear();
    row_ptr_1.clear();
    row_ind_1.clear();  
    off_diag_2.clear();
    col_ind_2.clear();
    row_ptr_2.clear();
    row_ind_2.clear();   
    unstable.clear();
    stable.clear();
    col_ind_s.clear();
    row_ind_s.clear();
    stable_value.clear();
    nuclides_original_order.clear();
    nuclides_full.clear();
}



void tran_mat::set_original_nuclide_order(vector<int> nuc_list){
    if(nuclides_original_order.empty()){
        for(int i=0; i<nuc_list.size(); i++){
           
            nuclides_original_order.push_back(nuc_list[i]);
        }     
    }

}

double tran_mat::get_original_conc(int zaid){
    for(int i=0; i<nuclides_full.size(); i++){
        if(zaid == nuclides_full[i]){
            return lib_ptr->original_nuclide_conc[i];
        }
    }
}

/* 
 * This function takes in the previously initialized instance of the 
 * library class and the reaction rates for the problem and constructs the 
 * transition matrix for solving the burnup equations
 */ 
void tran_mat::construct_transition_matrix(Library_builder* lib_class, vector< vector<double> > rxn_rates, vector<int> nuclides, bool rxns){
    
    
    double lambda;  // decay constant temp variable
    double l_ij;    // branching ration temp variable
    double f_ij;    // fission yield temp variable
    double nn_rate = 0.0; // non-fission reaction rate
    double value_temp = 0.0;
    
    lib_ptr = lib_class;
    
    
    int buff1;
    buff1=0;// checking where to pushback at end of row
    int buff2;
    buff2=0;// checking where to pushback at end of row
    row_ptr_1.push_back(0);
    row_ptr_2.push_back(0);
    
    bool row_started, row_started2; 
    row_started = false;
    row_started2 = false;
    bool stable_check = false;
    bool nn_rxn = false;
    int rxn_type;
    double removal_sum = 0.0;
    
    reactions = rxns;
    
    // loop through full matrix, checking to see if zero or not zero
    for( int i = 0; i< nuclides.size(); i++){
        for( int j = 0; j< nuclides.size(); j++){
            // check to see if diagonal element
            if(i==j){
                // calculate decay constant
                // stable, set to 0
                if(lib_class->get_hl_units(i) == 6){
                //    diag.push_back(0.0);
                    // leave loop if no reaction rates
                    stable.push_back(nuclides[i]);
                //    if(not rxns){ break;}
                    stable_check = true;
                }
                else{ stable_check = false;}
                if(not stable_check){
                    unstable.push_back(nuclides[i]);
                }
                // convert half life to correct units, and then to decay constant
                lambda = convert_to_lambda(lib_class->get_half_life(i), lib_class->get_hl_units(i));
                // check to see if reaction rates are present, add them in
                if(rxns){
                    // removal coefficient is sum of all reactions
                    for(int k=0; k < rxn_rates[i].size(); k++){
                        removal_sum += rxn_rates[lib_class->rxn_order_ptr[i]][k];
                    }
                    lambda = lambda + removal_sum;
                    removal_sum = 0.0;
                }
                // assign diagonal element
                //if(not stable_check) {
                    diag.push_back(-lambda);
                //}
               cout << "lambda for " << nuclides[i] << " = " << scientific << setprecision(15) <<  lambda << endl;
            }
            // if an off-diagonal element
            if( i != j ){
                // only push back vector element if non-zero
                // calculate branching fraction from j to i
                l_ij = branching_ratio_calculator(lib_class, nuclides[j], nuclides [i], j);
                // check to see if branching ratio is zero, if so assign to matrix
                if(fabs(l_ij) >= 1E-17){ 
                    lambda = convert_to_lambda(lib_class->get_half_life(j), lib_class->get_hl_units(j));
                    value_temp = lambda*l_ij;
                    // off_diag_1.push_back(lambda*l_ij);
                    buff1++;
                    if(not row_started){
                        row_started = true;
                    }
                    cout << "for nuclide " << j << " to nuclide " << i << " = " << l_ij*lambda << endl;
                    cout << "lij = " << l_ij << endl;
                    cout << "for nuclide " << nuclides[j] << " to nuclide " << nuclides[i] << " = " << l_ij*lambda << endl;
                //    col_ind_1.push_back(j); // push_back column index value
                //    row_ind_1.push_back(i); // push_back row index value
                }
                
                // if there is flux, find fission yield or addition neutron reactions
                if(rxns){
                    f_ij = fission_yield_calculator(lib_class, nuclides[j], nuclides [i], j,i);
                    // now add any probability of production from other rxns (non-fission)
                    nn_rxn = neutron_rxn_calc(lib_class, nuclides[j], nuclides [i],rxn_type);
                    if(nn_rxn){
                        /* 
                         * ADD LOGIC HERE FOR ADDING RXN RATES CORRECTLY
                         * Different vectors for each type of rxn (rxn_type)
                            * rxn_type == 1 --> (n,gamma)
                            * rxn_type == 2 --> (n,2n)    (-000010)
                            * rxn_type == 3 --> (n,3n)    (-000020)
                            * rxn_type == 4 --> (n,4n)    (-000030)
                            * rxn_type == 5 --> (n,p)     (-010000)
                            * rxn_type == 6 --> (n,d)     (-010010)
                            * rxn_type == 7 --> (n,t)     (-010020)
                            * rxn_type == 8 --> (n,He-3)  (-020020)
                            * rxn_type == 9 --> (n,alpha) (-020030)
                            * rxn_type == 10 --> Fission
                         * Note: all indices -1 because C++                  
                         */
                        if(rxn_type == 1){
                            nn_rate = rxn_rates[lib_class->rxn_order_ptr[j]][0];
                            cout << "found rxn from " << nuclides[j] << " to " << nuclides[i] << endl;
                            cout << "rxn rate = " << nn_rate << endl;
                        }
                        if(rxn_type == 2){
                            nn_rate = rxn_rates[lib_class->rxn_order_ptr[j]][1];
                        }
                        if(rxn_type == 3){
                            nn_rate = rxn_rates[lib_class->rxn_order_ptr[j]][2];
                        }
                        if(rxn_type == 4){
                            nn_rate = rxn_rates[lib_class->rxn_order_ptr[j]][3];
                        }
                        if(rxn_type == 5){
                            nn_rate = rxn_rates[lib_class->rxn_order_ptr[j]][4];
                        }
                        if(rxn_type == 6){
                            nn_rate = rxn_rates[lib_class->rxn_order_ptr[j]][6];
                        }
                        if(rxn_type == 7){
                            nn_rate = rxn_rates[lib_class->rxn_order_ptr[j]][7];
                        }
                        if(rxn_type == 8){
                            nn_rate = rxn_rates[lib_class->rxn_order_ptr[j]][8];
                        }
                        if(rxn_type == 9){
                            nn_rate = rxn_rates[lib_class->rxn_order_ptr[j]][9];
                        }
                        
                    }
                    if(not nn_rxn){
                        nn_rate = 0.0;
                    }
                    value_temp += nn_rate;
                    // do check to see if reaction yield is zero
                    if(fabs(f_ij) >= 1E-32){  // check this threshold
                        //off_diag_2.push_back(f_ij*rxn_rates[lib_class->rxn_order_ptr[j]][7]+nn_rate);
                        value_temp += f_ij*rxn_rates[lib_class->rxn_order_ptr[j]][6];
                        buff2++;
                        if(not row_started2){
                            row_started2 = true;
                        }
//                        col_ind_2.push_back(j); // push_back column index value
//                        row_ind_2.push_back(i);
                    }
                    
                }
                if(fabs(value_temp) > 1E-30){
                    off_diag_1.push_back(value_temp);
                    col_ind_1.push_back(j);
                    row_ind_1.push_back(i);
                    cout << "ASSIGNING " << value_temp << " to " << i << "," << j << endl;
                }
                value_temp = 0.0;
            }
        }
        if( i >= 0){
            if(not row_started){
                row_ptr_1.push_back(buff1);
               }
            if(row_started){
                row_ptr_1.push_back(buff1);
                }
            if(rxns and not row_started2){
                row_ptr_2.push_back(buff2);
            }
            if(rxns and row_started2){
                row_ptr_2.push_back(buff2);
           }  
          }
        row_started = false;
        row_started2 = false;
        nuclides_full.push_back(nuclides[i]);
    }
    // to signal that last row ends with diagonal element
    row_ptr_1.push_back(buff1);
    row_ptr_2.push_back(buff2);
    cout << "here is size of off_diag_1 == " << off_diag_1.size() << endl;
     cout << "size of row_ptr_1 = " << row_ptr_1.size() << endl;

}

double tran_mat::convert_to_lambda(double half_life, int hl_unit){
    
    double lambda;
    // half life unit of 1 is already seconds
    if(hl_unit == 1){ 
        lambda = log(2)/(half_life);
    }
    if(hl_unit == 2){ 
        lambda = log(2)/(half_life*60);  // minutes -> seconds
    }
    if(hl_unit == 3){
        lambda = log(2)/(half_life*3600);  // hours -> seconds
    }
    if(hl_unit == 4){
        lambda = log(2)/(half_life*86400); // days -> seconds
    }
    if(hl_unit == 5){
        lambda = log(2)/(half_life*31557600); // years -> seconds, assume 365.25 days/year
    }
    // hl_unit == 6 means stable isotope, need to reduce matrix
    if(hl_unit == 6){
        lambda = 0; // years -> seconds, assume 365.25 days/year
    }    
    if(hl_unit == 7){
        lambda = log(2)/(half_life*31557600000); // 1E3 years -> seconds, assume 365.25 days/year
    }
    if(hl_unit == 8){
        lambda = log(2)/(half_life*3.15576E13);  // 1E6 years -> seconds, assume 365.25 days/year
    }
    if(hl_unit == 9){
        lambda = log(2)/(half_life*3.15576E16); // 1E9 years -> seconds, assume 365.25 days/year
    }
     
    return lambda;
    
}

double tran_mat::branching_ratio_calculator(Library_builder* lib_class, int n_i, int n_j, int ind){
   /* possible avenues of decay to check (n_i to n_j)
    * 1. beta decay to ground state (+10000)
    * 2. beta decay to metastable (+10001)
    * 3. positron emission/electron capture to ground (-10000)
    * 4. positron emission/electron capture to metastable (-10000 + 1)
    * 5. alpha decay (-20040)
    * 6. isomeric transition (+/- 1)
    * 7. delayed neutron decay (-10)
    * 8. neutron decay (-10)
    * 9. double beta decay (not sure?) (+20000) or (-20)
    * 10. beta+alpha decay (-20040 and +10000 = -10040)
    */
    
    double branch_ratio=0;
    
    // 1. beta decay to ground state (+10000)
    if(n_i + 10000 == n_j){
        branch_ratio += lib_class->get_beta_1(ind);
    }
    // 2. beta decay to metastable (+10001)
    if(n_i + 10001 == n_j){
        branch_ratio += lib_class->get_beta_2(ind);
    }    
    // 3. positron emission/electron capture to ground (-10000)
    if(n_i - 10000 == n_j){
        branch_ratio += lib_class->get_posit_1(ind);
    }    
    // 4. positron emission/electron capture to metastable (-10000 + 1)
    if(n_i - 9999 == n_j){
        branch_ratio += lib_class->get_posit_2(ind);
    }
    // 5. alpha decay (-20040)
    if(n_i - 20040 == n_j){
        branch_ratio += lib_class->get_alpha(ind);
    }
    // 6. isomeric transition (+/- 1)
    if((n_i - 1 == n_j) or ((n_i + 1 == n_j))){
        branch_ratio += lib_class->get_isomer(ind);
    }
    // 7. delayed neutron decay (-10)
    if(n_i - 10 == n_j){
        branch_ratio += lib_class->get_delayed(ind);
    }
    // 8. neutron decay (-10)
    if(n_i - 10 == n_j){
        branch_ratio += lib_class->get_neutron_decay(ind);
    }
    // 9. double beta decay (not sure?) (+20000) or (-20)
    if(n_i + 20000 == n_j){
        branch_ratio += lib_class->get_beta_double(ind);
    }
    // 10. beta+alpha decay (-20040 and +10000 = -10040)
    if(n_i - 10040 == n_j){
        branch_ratio += lib_class->get_beta_alpha(ind);
    }  
    
    return branch_ratio;
}

bool tran_mat::neutron_rxn_calc(Library_builder* lib, int n_i, int n_j, int& rxn_type){
   /* column neutron-nuclide interactions (n_i to n_j)
    * 1. (n,gamma) (+000010)
    * 2. (n,2n)    (-000010)
    * 3. (n,3n)    (-000020)
    * 4. (n,4n)    (-000030)
    * 5. (n,p)     (-010000)
    * 6. (n,d)     (-010010)
    * 7. (n,t)     (-010020)
    * 8. (n,He-3)  (-020020)
    * 9. (n,alpha) (-020030)
    * 10. Fission
    */
    
    double rxn = false;
    
    // 1. (n,gamma) (+000010)
    if(n_i + 10 == n_j){
        rxn = true;
        rxn_type = 1;
    }
    // 2. (n,2n)    (-000010)
    if(n_i - 10 == n_j){
        rxn = true;
        rxn_type = 2;
    }
    // 3. (n,3n)    (-000020)
    if(n_i - 20 == n_j){
        rxn = true;
        rxn_type = 3;
    }
    // 4. (n,3n)
    if(n_i - 30 == n_j){
        rxn = true;
        rxn_type = 4;
    }
    // 5. (n,p)     (-010000)
    if(n_i - 10000 == n_j){
        rxn = true;
        rxn_type = 5;
    }
    // 6. (n,d)     (-010010)
    if(n_i - 10010 == n_j){
        rxn = true;
        rxn_type = 6;
    }
    // 7. (n,t)     (-010020)
    if(n_i - 10020 == n_j){
        rxn = true;
        rxn_type = 7;
    }
    // 8. (n,He-3)  (-020020)
    if(n_i - 20020 == n_j){
        rxn = true;
        rxn_type = 8;
    }
    // 9. (n,alpha) (-020030)
    if(n_i - 20030 == n_j){
        rxn = true;
        rxn_type = 9;
    }
    
    return rxn;
}

double tran_mat::fission_yield_calculator(Library_builder* lib_class, int n_i, int n_j, int i, int j){
    
    // logical to check if there is a fission yield to look up from i to j
    bool yield_exists = false;
    double yield;
    // first, check to see if nuclide i is in fission database
    for(int k = 0; k < lib_class->get_number_of_fiss(); k++){
        // check to see if fissionable nuclide exists
        if(i == lib_class->get_fission_nuc_ind(k)){
            // cout << "found a fission nuclide for the matrix!!!" << endl;
            // now check to see if fission product corresponds
            for(int h = 0; h < lib_class->get_number_of_fp(); h++){
                if( j == lib_class->get_fission_prod_ind(h)){
                  //  cout << "found a fission product for the matrix!!!" << endl;
                  //  cout << "fission from " << n_i << " to " << n_j << " = " << lib_class->get_yield_frac(k,h) << endl;
                    yield = lib_class->get_yield_frac(k,h);
                    yield_exists = true;
                    break;
                }
            }  
        }
    }
    if(yield_exists){
        return yield;
    }
    else{
        return 0.0;
    }
}


 /*
  * Function to print out transition matrix in form for MATLAB to analyze
  * behavior. Prints out CSR format and row x column index format.
  */
void tran_mat::print_transition_matrix( vector<int> nuclides, vector<double> conc_i_u, vector<double> conc_i_s, const char * output_file){
    // right now, only prints out MATLAB format
    // MATLAB function spconvert takes in the following format:
    // columns [i,j,re] of D to construct S, such that S(i(k), j(k)) = re(k)
    /*
     * Example output:
     * 1    1    1.000000000000000
     * 1    2    0.500000000000000
     * 2    2    0.333333333333333
     * 1    3    0.333333333333333
     * 2    3    0.250000000000000
     * 3    3    0.200000000000000
     * 3    3    0.000000000000000
     * 
     * Last line is size of matrix and 0.000
     */

    //ofstream outfile(output_file);
    ofstream outfile2(output_file);
    ofstream outfile3("nuclides_classifier.dat");
    ofstream outfile4("stable_matrix.dat");
    ofstream outfile5("concentrations_unstable.dat");
    ofstream outfile6("concentrations_stable.dat");
    
    int col_iter=0;
    int row_iter=0;
    
    cout << "STARTING PRINT NOW" << endl;
    
  /*  for(int i = 0; i<nuclides.size(); i++){
        row_iter = row_ptr_1[i+1] - row_ptr_1[i];
        if(row_iter == 0 ){
            outfile << i+1 << "   " << i+1 << "    " << scientific << setprecision(15) << diag[i] << endl;
        }
        else{
                outfile << i+1 << "   " << i+1 << "    " << scientific << setprecision(15) << diag[i] << endl;          
            do{
                outfile << i+1 << "   " << col_ind_1[col_iter]+1 << "    " << scientific << setprecision(15) << off_diag_1[col_iter] << endl;
                col_iter++;
                row_iter--;
            }while(row_iter > 0 );  
        }
        
    }
   */
col_iter=0;   

    for(int i=0; i<conc_i_u.size(); i++){
        for(int j=0; j<conc_i_u.size(); j++){             
            if(i==j){
                outfile2 << i+1 << "   " << i+1 << "    " << scientific << setprecision(15) << diag[i] << endl;
            }
            if(not col_ind_1.empty()){
                if(col_ind_1[col_iter] == j and row_ind_1[col_iter] == i){
                    outfile2 << row_ind_1[col_iter]+1 << "   " << col_ind_1[col_iter]+1 << "    " << scientific << setprecision(15) << off_diag_1[col_iter] << endl;
                    col_iter++;
                }   
            }
        }
    }
  
 
   // outfile << nuclides.size() << "   " << nuclides.size() << "    " << 0.000000 << endl;
    outfile2 << conc_i_u.size() << "   " << conc_i_u.size() << "    " << 0.000000 << endl;

    outfile3 << "Full List of isotopes " << endl;
    for(int i=0; i< nuclides.size(); i++){
        outfile3 << i+1 << ". " << nuclides_full[i] << endl;
    }
    
    outfile3 << "\nList of " << unstable.size() << " unstable isotopes " << endl;
    for(int i=0; i< unstable.size(); i++){
        outfile3 << unstable[i] << endl;
    }
    
    outfile3 << "\nList of " << stable.size() << " stable isotopes " << endl;
    for(int i=0; i< stable.size(); i++){
        outfile3 << stable[i] << endl;
    }
    
    outfile4 << "Stable isotope matrix" << endl;
    for(int i =0; i < conc_i_s.size(); i++){
        outfile4 << row_ind_s[i]+1 << "   " << col_ind_s[i]+1 << "    " << stable_value[i] << endl;
    }
    
    outfile5 << "Unstable isotope matrix, initial concentrations" << endl;
    if(conc_i_u.size() > 0){
        for(int i =0; i < conc_i_u.size(); i++){
            outfile5 << conc_i_u[i] << endl;
        }
    }
    
    outfile6 << "Stable isotope matrix, initial concentrations" << endl;
    if(conc_i_s.size() > 0){
        for(int i =0; i < conc_i_s.size(); i++){
            outfile6 << conc_i_s[i] << endl;
        }
    }
        
    cout << "SUCCESSFUL PRINT?" << endl;
}

void tran_mat::reduce_transition_matrix(vector<int> nuclides, bool rxns, vector<double>& conc_i, vector<double>& conc_i_unstable, vector<double>& conc_i_stable, bool& reduce){
    
    reduce=false;
    bool reduced = false;
    int iter = 0;
    
    cout << "sizes before reduction: " << endl;
    cout << "diag = " << diag.size() << endl;
    cout << "off_diag = " << off_diag_1.size() << endl;

     
    // check to see if matrix needs to be reduced if no reactions
//    if(not rxns){
        for(int i=0; i<nuclides.size(); i++){
            cout << "checking diag before reduce" << endl;
            cout << "diag[" << i << "] = " << diag[i] << endl;
            if(fabs(diag[i]) < 1E-32){
                reduce = true;
                reduced = true;
                // remove elements from transition matrix and move to stable isotope storage
                // diagonal element first
                cout << "REDUCER CHECK!!!" << endl;
                cout << "diag[" << i << "] = " << diag[i] << endl;
                stable_value.push_back(diag[i]);
                col_ind_s.push_back(i);
                row_ind_s.push_back(i);
                conc_i_stable.push_back(conc_i[iter]);
                diag.erase(diag.begin() + i);
                // now remove all off diagonal elements
                for(int j =0; j<col_ind_1.size(); j++){
                    if(col_ind_1[j] == i || row_ind_1[j] == i){
                        cout << "Reducing offd" << endl;
                        stable_value.push_back(off_diag_1[j]);
                        if( j > i){
                            col_ind_s.push_back(col_ind_1[j]-1);
                        }
                        if(j <= i){
                            col_ind_s.push_back(col_ind_1[j]);
                        }
                        row_ind_s.push_back(row_ind_1[j]);
                        off_diag_1.erase(off_diag_1.begin()+j);
                        col_ind_1.erase(col_ind_1.begin()+j);
                        row_ind_1.erase(row_ind_1.begin()+j);
                    }
                    if(col_ind_1[j] > i){
                        col_ind_1[j] -= 1;
                    }
                    if(row_ind_1[j] > i){
                        row_ind_1[j] -= 1;
                    }
                }
                i--;
            }
            if(fabs(diag[i]) > 1E-32 and not reduced){
                conc_i_unstable.push_back(conc_i[iter]);
            }
            reduced = false;
            iter++;
            if(iter >= nuclides.size()){break;}
        }
//    }
    
//    // if reactions exist, then check to see if diagonal element is zero
//    if(rxns){
//        for(int i=0; i<nuclides.size(); i++){
//            if(fabs(diag[i]) < 1E-32){  // and fabs(rxn_rates[i]) < 1E-32){
//                reduce = true;
//                reduced = true;
//                // remove elements from transition matrix and move to stable isotope storage
//                // diagonal element first
//                cout << "REDUCER CHECK!!!" << endl;
//                cout << "diag[" << i << "] = " << diag[i] << endl;
//                stable_value.push_back(diag[i]);
//                col_ind_s.push_back(i);
//                row_ind_s.push_back(i);
//                conc_i_stable.push_back(conc_i[iter]);
//                diag.erase(diag.begin()+i);
//                
//                // now remove all off diagonal elements
//                for(int j =0; j<col_ind_1.size(); j++){
//                    if(col_ind_1[j] == i || row_ind_1[j] == i){
//                        stable_value.push_back(off_diag_1[j]);  
//                        if( j > i){
//                            col_ind_s.push_back(col_ind_1[j]-1);
//                        }
//                        if(j <= i){
//                            col_ind_s.push_back(col_ind_1[j]);
//                        }
//                        row_ind_s.push_back(row_ind_1[j]);
//                        off_diag_2.erase(off_diag_1.begin()+j);
//                        col_ind_2.erase(col_ind_1.begin()+j);
//                        row_ind_2.erase(row_ind_1.begin()+j);
//                    }
//                    if(col_ind_1[j] > i){
//                        col_ind_1[j] -= 1;
//                        
//                    }
//                    if(row_ind_1[j] > i){
//                        row_ind_1[j] -= 1;
//                    }
//                }
//                i--;
//            }
//            if(fabs(diag[i]) < 1E-32 and not reduced){ // and fabs(rxn_rates[i] < 1E-32
//                conc_i_unstable.push_back(conc_i[iter]);
//            }
//            reduced = false;
//            iter++;
//            if(iter >= nuclides.size()){break;}
//        }
//    }
//    
     cout << "sizes after reduction: " << endl;
     cout << "diag = " << diag.size() << endl;
     cout << "off_diag = " << off_diag_1.size() << endl; 
//     for(int i = 0; i<off_diag_1.size(); i++){
//         cout << row_ind_1[i]+1 << "   " << col_ind_1[i]+1 << "    " << scientific << setprecision(15) << off_diag_1[i] << endl;
//     }
     for(int i = 0; i<stable_value.size(); i++){
         cout << row_ind_s[i]+1 << "   " << col_ind_s[i]+1 << "    " << scientific << setprecision(15) << stable_value[i] << endl;
     }

        
    if(not reduce){
        cout << "Transition matrix full, no reduction needed." << endl;
    }
    else{
        cout << "Transition matrix successfully reduced " << endl;
    }
}

