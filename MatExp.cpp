/* 
 * File:   matexp.cpp
 * Author: dlago_000
 *
 * Created on June 5, 2015, 2:39 PM
 */

#include "tran_mat.h"
#include "MatExp.h"
#include <cstdlib>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include "math.h"
#include "stdio.h"

using namespace std;

/*
 * This function takes in the instance to the transition matrix and calculates 
 * the solution to the burnup equations using the matrix exponential method.
 * Inputs:
 * 1. Instance to transition matrix class
 * 2. Initial nuclide concentration
 * 3. Final nuclide concentration vector (to populate)
 * 4. Time interval
 * 5. Tolerance for convergence 
 */
void mat_exp_solver( tran_mat* A, vector<double>& N_i, vector<double>& N_f,  vector<double> rxn_rates, double time, double tol){
    
    cout << "Made it to the solver my friends!!" << endl;
    // set temporary vectors for calculations and 
    vector<double> Ca;
    vector<double> Cb;
    Cb.reserve(N_i.size());
    double temp;
    vector<double> convergence_factor;
    int iter=0;
    int row_iter;
    int counter = 0;
    // set Ca = initial concentration
    copy_conc(N_i,Ca);
    // continue iteration until converged
    // for first iterate, use initial concentration
    do{
        // loop through each row
        for(int i=0; i < N_i.size(); i++){
            //check to see how many elements in row to be summed
            row_iter = A->row_ptr_1[i+1] - A->row_ptr_1[i];
           
            // if only diagonal element
            if(row_iter == 0 ){
                temp = A->diag[i]*Ca[i];
            }
            // if row has more elements than diagonal
            else{
                // add diagonal element
                temp = A->diag[i]*Ca[i];
                do{
                    temp += A->off_diag_1[counter]*Ca[A->col_ind_1[counter]];
                    cout << "value in off diag = " << A->off_diag_1[counter]*Ca[A->col_ind_1[counter]] << endl;
                    counter++;
                    row_iter--;
                }while(row_iter > 0 );
            }
            // set second vector
            if(iter == 0){
                Cb[i] = (time/(iter+1))*temp + N_i[i];
                N_f.push_back(0.0);
            }
            else{
                Cb[i] = (time/(iter+1))*temp;
            }
            iter++;
            N_f[i] += Cb[i];
        }
        // check for convergence
        convergence_factor.push_back(convergence_check(Cb,Ca));
        cout << "Conv. Factor = " << convergence_factor[iter-1] << endl;
        // set Ca == Cb
        copy_conc(Cb,Ca);
    }while( iter < 10);// convergence_factor[iter-1] >= tol);
    
    
    
}


void copy_conc(vector<double> N_i, vector<double>& Ca){
    
    for(int i=0; i<N_i.size(); i++){
        Ca.push_back(N_i[i]);
    }
}

double convergence_check(vector<double> V1, vector<double> V2){
    // loop through vectors, see what max difference is in percent
    // return largest max % relative difference
    double temp1=0;
    double temp2=0;
    for( int i = 0; i < V1.size(); i++){
        temp2 = fabs((V1[i]-V2[i])/V1[i]);
        if( temp2 > temp1){
            temp1 = temp2;
        }
    }
    return temp1;
}