/* 
 * File:   cram.h
 * Author: Daniel
 *
 * Created on August 21, 2015, 1:30 PM
 */
using namespace std;

#include<math.h>
#include<vector>
#include<limits>
#include "depletion.h"

#ifndef CRAM_H
#define	CRAM_H


class cram {
public:
    // constructor
    cram();
    // destructor
    virtual ~cram();
    
    /* 
     * This function contains a solver for the burnup equations
     * using the Chebyshev Rational Approximate Method (CRAM)
     * and the accompanying function to produce the pertinent coefficients.
     * cram_solver_1 uses the contour integrals and quadrature rule to generate
     * partial fraction coefficients for the rational approximation.
     * Input:
     * k = order of approximation (must be even)
     */
    void cram_solver_1(depletion*, tran_mat*, int k);
    
    /* 
     * This function contains generates the partial fraction coefficients
     * for a rational approximation applied to the contour integral 
     * representation of the approximation.
     * Input: 
     * k = order of approximation (must be even)
     * Output:
     * theta = poles of rational function r
     * alpha = residues at poles
     * alpha_0 = limit as r-> inf
     */
    void quadrature_coefficients(int k);    
    
    /* 
     * This function contains a solver for the burnup equations
     * using the Chebyshev Rational Approximate Method (CRAM)
     * and the accompanying function to produce the pertinent coefficients.
     * cram_solver_2 uses the precomputed coefficients and implements
     * the  Carathéodory–Fejér method to efficiently and quickly converge
     * to the new concentration vector.
     */
    void cram_solver_2();
    
    /* 
     * This function is a Gauss-Seidel solver for a sparse matrix
     * NOTE: This only converges for matrices that are strictly diagonally dominant
     * 1. solution vector (x)
     * 2. transition matrix parameters (A)
     * 3. diagonals (temporary vector) (A)
     * 4. off diagonals (temporary vector) (A)
     * 5. initial concentration (b)
     *
     * 
     */
    void sparse_solve_GS(vector<double>& , tran_mat*, vector<double>, vector<double>, vector<double>, int ind);
    
    /* 
     * This function is an LU solver for a sparse matrix
     * This function finds the LU decomposition of a matrix and solves:
     * Ax = b
     * LU =A 
     * LUx = b --> Ly = b --> Ux = y
     * 1. solution vector (x)
     * 2. transition matrix parameters (A)
     * 3. diagonals (temporary vector) (A)
     * 4. off diagonals (temporary vector) (A)
     * 5. initial concentration (b)
     *
     * 
     */
    void LU_solve(vector<double>& , tran_mat*, vector<double>, vector<double>, vector<double>, int ind);
    
private:
    /*
     * theta = poles of rational function r
     * alpha = residues at poles
     * alpha_0 = limit as r-> inf
     */
    
    vector<double> buffer1, buffer2;
    vector<double> conc_f;
    
    vector<double> theta_real, theta_cmplx;
    vector<double> alpha_real, alpha_cmplx;
    double alpha_0;
    
};

#endif	/* CRAM_H */

