/* 
 * File:   stable_solve.h
 * Author: Daniel
 *
 * Created on February 2, 2016, 12:58 PM
 * 
 * This class solves for the concentration of the 
 * nuclides reduced from the original transition matrix
 * using a linear chain method.
 * 
 */


#include<math.h>
#include<vector>
#include<limits>
#include "depletion.h"



using namespace std;

#ifndef STABLE_SOLVE_H
#define	STABLE_SOLVE_H

/*
 * This class contains the tools and functions to run to solve
 * for the final concentrations of nuclides reduced from the
 * transition matrix.
 * 
 */
class stable_solve {
public:
    // constructor
    stable_solve();
    
    /*
     * This function solves for the nuclide concentration if
     * a flux is present, using the generalized form of the 
     * batch Bateman equations given a linear chain. 
     * 
     */
    void batch_solve1(depletion*, tran_mat*, vector<double>&);
    
    /*
     * This function solves for the nuclide concentration if
     * NO flux is present, using the generalized form of the 
     * batch Bateman equations given a linear chain. 
     * 
     */    
    void batch_solve2(depletion*, tran_mat*, vector<double>&);
    
    // destructor
    virtual ~stable_solve();
    
private:

};

#endif	/* STABLE_SOLVE_H */

