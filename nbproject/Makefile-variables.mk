#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=Cygwin_4.x-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/Cygwin_4.x-Windows
CND_ARTIFACT_NAME_Debug=depletion_module_v1.0
CND_ARTIFACT_PATH_Debug=dist/Debug/Cygwin_4.x-Windows/depletion_module_v1.0
CND_PACKAGE_DIR_Debug=dist/Debug/Cygwin_4.x-Windows/package
CND_PACKAGE_NAME_Debug=depletionmodulev1.0.tar
CND_PACKAGE_PATH_Debug=dist/Debug/Cygwin_4.x-Windows/package/depletionmodulev1.0.tar
# Release configuration
CND_PLATFORM_Release=Cygwin_4.x-Windows
CND_ARTIFACT_DIR_Release=dist/Release/Cygwin_4.x-Windows
CND_ARTIFACT_NAME_Release=depletion_module_v1.0
CND_ARTIFACT_PATH_Release=dist/Release/Cygwin_4.x-Windows/depletion_module_v1.0
CND_PACKAGE_DIR_Release=dist/Release/Cygwin_4.x-Windows/package
CND_PACKAGE_NAME_Release=depletionmodulev1.0.tar
CND_PACKAGE_PATH_Release=dist/Release/Cygwin_4.x-Windows/package/depletionmodulev1.0.tar
# Remote configuration
CND_PLATFORM_Remote=GNU-Linux-x86
CND_ARTIFACT_DIR_Remote=dist/Remote/GNU-Linux-x86
CND_ARTIFACT_NAME_Remote=depletion_module_v1.0
CND_ARTIFACT_PATH_Remote=dist/Remote/GNU-Linux-x86/depletion_module_v1.0
CND_PACKAGE_DIR_Remote=dist/Remote/GNU-Linux-x86/package
CND_PACKAGE_NAME_Remote=depletionmodulev1.0.tar
CND_PACKAGE_PATH_Remote=dist/Remote/GNU-Linux-x86/package/depletionmodulev1.0.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
