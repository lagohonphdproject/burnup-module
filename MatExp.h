/* 
 * File:   matexp.h
 * Author: dlago_000
 *
 * Created on June 5, 2015, 2:39 PM
 */

#include "tran_mat.h"

#ifndef MATEXP_H
#define	MATEXP_H

/*
 * This function initializes the necessary variables for use in the matexp 
 * solver.
 */
void initialize_solver();

/*
 * This function takes in the instance to the transition matrix and calculates 
 * the solution to the burnup equations using the matrix exponential method.
 * Inputs:
 * 1. Instance to transition matrix class
 * 2. Initial nuclide concentration
 * 3. Final nuclide concentration vector (to populate)
 * 4. Time interval
 * 5. Tolerance for convergence 
 */
void mat_exp_solver( tran_mat* , vector<double>& , vector<double>& , vector<double>, double, double );


/*
 * This function sets Ca to be the initial concentration vector
 */
void copy_conc(vector<double>, vector<double>&);

/*
 * This function does a convergence check between the two successive
 * vectors used to solve the burnup equations in matexp.
 * 
 */
double convergence_check(vector<double>, vector<double>);
#endif	/* MATEXP_H */

