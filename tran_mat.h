/* 
 * File:   tran_mat.h
 * Author: dlago_000
 *
 * Created on July 10, 2015, 2:47 PM
 */

#include "library_builder.h"
#include <vector>
#include <string>

using namespace std;

#ifndef TRAN_MAT_H
#define	TRAN_MAT_H


/*
 * This class contains the transition matrix used to solve the burnup equations
 * 
 */
class tran_mat {
public:
    
    // constructor
    tran_mat();
    // destructor
    virtual ~tran_mat();
    
    public:

    /* the following vectors make up the transition matrix
     * the values are stored in a compressed sparse row format
     * the diagonal elements are all stored in one vector, and the
     * off diagonal elements are stored in the CSR format.
     * The off diagonal elements are stored in two separate vectors
     * One vector contains the values without rxn rates, and the other 
     * vector holds the values related to the rxn rates
     */
        
        
    // boolean signifying rxns or no rxns
    bool reactions;
    // internally store list of tracked nuclides
    vector<int> nuclides_full;
    vector<int> nuclides_original_order;
    // diagonal elements in transition matrix
    // -(lambda_i + sigma_i * phi) 
    // only store lambda_i, add rxn rates as they are provided
    vector<double> diag;
    // first set of CSR-format off diagonal elements
    // (l_ij * lambda_j)
    // CSR format is as follows
    // value
    vector<double> off_diag_1;
    // column index 
    vector<int> col_ind_1;
    // if row_ptr[i] = 0, then row only has diagonal element, diag[i]
    // i.e. the first value in row j is off_diag_1[row_ptr_1[j] - 1]
    vector<int> row_ptr_1;
    vector<int> row_ind_1;  // temp vector for now
    // second set of CSR-format off diagonal elements
    // (f_ij) --> multiply by rxn rate
    // CSR format is as follows
    // value
    vector<double> off_diag_2;
    // column index 
    vector<int> col_ind_2;
    // row pointer, where each row starts in column vector
    // if row_ptr[i] = 0, then row starts with diagonal element, diag[i]
    vector<int> row_ptr_2;
    vector<int> row_ind_2;  // temp vector for now    
    
    // vector to keep all unstable isotopes for transition matrix
    vector<int> unstable;
    // vector to keep and track all stable isotopes (pure buildup)
    vector<int> stable;
    
    // vectors for storing reduced elements in transition matrix
    // column index of stable isotope reduced
    vector<int> col_ind_s;
    // row index of stable isotope reduced
    vector<int> row_ind_s;
    // value of stored element reduced
    vector<double> stable_value;
    
    // hold ptr to Library class
    
    Library_builder* lib_ptr;
    
    // function to get original concentration vector given a ZAID
    double get_original_conc(int);
    
    void set_original_nuclide_order(vector<int>);
    
    /*
     * Function to create initial transition matrix
     * takes in rxn rates and pointer to instance of library class 
     * also takes in boolean to see if reactions are included in problem, decide
     * whether or not to store addition memory for fission yields     
     */
    void construct_transition_matrix(Library_builder*, vector< vector<double> >, vector<int>, bool);
    
    /*
     * Function to convert half-life to lambda
     * takes in half-life, and unit type to convert     
     */
    double convert_to_lambda(double , int );
    
    /*
     * Function to calculate the decay branching ratio of nuclide i to nuclide j
     * takes in each nuclide, does the logic to check branching ratios
     * takes in library builder class to get values     
     */
    double branching_ratio_calculator(Library_builder*, int, int, int);
    
    /*
     * Function to calculate the branching ratio of nuclide i to nuclide j
     * for non-fission, neutron-nuclide interactions (e.g. (n,2n))
     * takes in each nuclide, does the logic to check branching ratios
     * takes in library builder class to get values     
     */
    bool neutron_rxn_calc(Library_builder*, int, int, int&);
    
    /*
     * Function to calculate the fission yield fraction of nuclide i to nuclide j
     * takes in each nuclide, does the logic to yield ratios
     * takes in library builder class to get values     
     */
    double fission_yield_calculator(Library_builder*, int, int, int, int);

    /*
     * Function to print out transition matrix in form for MATLAB to analyze
     * behavior. Prints out CSR format and row x column index format.
     */
    void print_transition_matrix( vector<int>, vector<double>, vector<double>, const char * );
    
    /*
     * Function to reduce transition matrix in case of stable elements
     * and absence of flux (i.e. reduce an underdefined matrix to make system
     * solvable)
     */
    void reduce_transition_matrix(vector<int>, bool, vector<double>&, vector<double>&, vector<double>&, bool&);
    
private:

};

#endif	/* TRAN_MAT_H */

