/* 
 * File:   Library_builder.cpp
 * Author: dlago_000
 * 
 * Created on July 10, 2015, 2:32 PM
 */

#include "Library_builder.h"
#include <cstdlib>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include "math.h"
#include "stdio.h"
#include <stdexcept>    // for throwing exceptions/error handling

using namespace std;
// Constructor initializes library class
Library_builder::Library_builder() {
    // initialize necessary objects
    // build fissionable isotope arrays
        fiss_array.push_back(902270); // Th227
        fiss_array.push_back(902290); // Th229
        fiss_array.push_back(902320); // Th232
        fiss_array.push_back(912310); // Pa231
        fiss_array.push_back(922320); // U232
        fiss_array.push_back(922330); // U233
        fiss_array.push_back(922340); // U234
        fiss_array.push_back(922350); // U235
        fiss_array.push_back(922360); // U236
        fiss_array.push_back(922370); // U237
        fiss_array.push_back(922380); // U238
        fiss_array.push_back(932370); // Np237
        fiss_array.push_back(932380); // Np238
        fiss_array.push_back(942380); // Pu238
        fiss_array.push_back(942390); // Pu239
        fiss_array.push_back(942400); // Pu240
        fiss_array.push_back(942410); // Pu241
        fiss_array.push_back(942420); // Pu242
        fiss_array.push_back(952410); // Am241
        fiss_array.push_back(952421); // Am242m
        fiss_array.push_back(952430); // Am243
        fiss_array.push_back(962420); // Cm242
        fiss_array.push_back(962430); // Cm243
        fiss_array.push_back(962440); // Cm240
        fiss_array.push_back(962450); // Cm245
        fiss_array.push_back(962460); // Cm246
        fiss_array.push_back(962480); // Cm248
        fiss_array.push_back(982490); // Cf249
        fiss_array.push_back(982510); // Cf251
        fiss_array.push_back(992540); // Es254 
        
        // setup fission yield energy boundaries
        fiss_energies.push_back(2.53000E-02);
        fiss_energies.push_back(2.00000E+06);
        fiss_energies.push_back(1.40000E+07);
            
}


// Destructor clears arrays
Library_builder::~Library_builder() {
    // clear arrays
    lib_type.clear();
    hl_units.clear();
    half_life.clear();
    beta_1.clear();
    beta_2.clear();
    posit_1.clear();
    posit_2.clear();
    alpha.clear();
    isomer.clear();
    spont_fiss.clear();
    delayed.clear();
    decay_heat.clear();
    recover_gx.clear();
    nat_abund.clear();
    water_rcg.clear();
    air_rcg.clear();
    beta_double.clear();
    neutron_decay.clear();
    beta_alpha.clear();
    lib_pos.clear();
    fiss_array.clear();
    fiss_energies.clear();
    fission_nuc_ind.clear();
    fission_prod_ind.clear();
    yield_frac.clear();
}

/*
 * The following set of get functions are ways to retrive the protected values
 * in this class. The values should be set buy the decay library files and 
 * not the user.
 */
    int Library_builder::get_lib_type(int index){
        return lib_type[index];
    }

    int Library_builder::get_hl_units(int index){
        return hl_units[index];
    }
    
    double Library_builder::get_half_life(int index){
        return half_life[index];
    }

    double Library_builder::get_beta_1(int index){
        return beta_1[index];
    }

    double Library_builder::get_beta_2(int index){
        return beta_2[index];
    }
    
    double Library_builder::get_posit_1(int index){
        return posit_1[index];
    }
    
    double Library_builder::get_posit_2(int index){
        return posit_2[index];
    }
    
    double Library_builder::get_alpha(int index){
        return alpha[index];
    }
    
    double Library_builder::get_isomer(int index){
        return isomer[index];
    }

    double Library_builder::get_spont_fiss(int index){
        return spont_fiss[index];
    }
    
    double Library_builder::get_delayed(int index){
        return delayed[index];
    }
    
    double Library_builder::get_decay_heat(int index){
        return decay_heat[index];
    }

    double Library_builder::get_recover_gx(int index){
        return recover_gx[index];
    }
    
    double Library_builder::get_nat_abund(int index){
        return nat_abund[index];
    }
    
    double Library_builder::get_water_rcg(int index){
        return water_rcg[index];
    }
    
    double Library_builder::get_air_rcg(int index){
        return air_rcg[index];
    }
    
    double Library_builder::get_beta_double(int index){
        return beta_double[index];
    }
    
    double Library_builder::get_neutron_decay(int index){
        return neutron_decay[index];
    }
    
    double Library_builder::get_beta_alpha(int index){
        return beta_alpha[index];
    }
    
    double Library_builder::get_lib_pos(int index){
        return lib_pos[index];
    };
 
    double Library_builder::get_fiss_array(int index){
        return fiss_array[index];
    }
    
    double Library_builder::get_fission_nuc_ind(int index){
        return fission_nuc_ind[index];
    }

    double Library_builder::get_fission_prod_ind(int index){
        return fission_prod_ind[index];
    }

    double Library_builder::get_yield_frac(int x, int y){
        return yield_frac[x][y];
    }

    int Library_builder::get_number_of_fiss(){
        return number_of_fiss;
    }
    
    int Library_builder::get_number_of_fp(){
        return number_of_fp;
    }    
    
/*
 * This function sorts the nuclide array for ease of use throughout library read
 * bubblesort
 */
void Library_builder::sort(vector<int>& nuclides, vector<double>& conc, int n){
      bool swapped = true;
      int j = 0;
      int temp;
      double temp2;
      while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < n - j; i++) {
                  if (nuclides[i] > nuclides[i + 1]) {
                        temp = nuclides[i];
                        temp2 = conc[i];
                        nuclides[i] = nuclides[i + 1];
                        nuclides[i + 1] = temp;
                        conc[i] = conc[i + 1];
                        conc[i + 1] = temp2;                        
                        swapped = true;
                  }
            }
      }
}


/* 
 * This function calls the library reader functions
 */ 
void Library_builder::read_library_files(vector<int>& nuclides, vector<double>& conc, vector<double> Avg_FE){
    
    // sort nuclides array
    sort(nuclides, conc, nuclides.size());
    
    try{
        read_decay_lib(nuclides); 
    }
    // check to see if all isotopes valid
    catch(logic_error& e){
      cerr << e.what() << endl;
      // end program if 
      cout << "ENDING PROGRAM, REMOVE INVALID ISOTOPE!" << endl;
      exit(EXIT_FAILURE);
      //return -1; 
    };
    
    // call decay library reader
    read_decay_lib(nuclides); 
    // call fission yield library reader
    read_yield_lib(nuclides, Avg_FE);
    
}

 /* 
 * This function reads in the initial list of nuclides for burnup tracking
 * and reads in the corresponding decay library information from the 
 * Origen decay library
 */ 
void Library_builder::read_decay_lib(vector<int> nuclides){
    
    // open text files with decay data 
    // extracted from ORIGEN
    ifstream infile1("origen.rev03.decay.data"); 
    // dummy strings for reading in files
    string line;
    string word;
    int num1;
    double num2;
    stringstream ss;
    int i=0;
    int fiss_prod_pos=0;
    bool found_fiss_prod = false;
    bool exists_in_library = false;
    // read in decay library files
    while (getline(infile1, line)){
        if (line.length() > 10){
            ss.clear();
            ss.str(line);
            ss >> num1; // read in first item
            ss >> word; // read in second item 
            if(atoi(word.c_str()) == nuclides[i]){
                cout << "found nuclide " << atoi(word.c_str()) << endl;
                exists_in_library = true;
                // now read in decay library data
                lib_type.push_back(num1);   // library type
                ss >> num1;
                hl_units.push_back(num1);  // half life units
                ss >> num2;
                half_life.push_back(num2);  // half life
                ss >> num2;
                beta_2.push_back(num2);  // beta decay to metastable
                ss >> num2;
                posit_1.push_back(num2);  // positron to ground
                ss >> num2;
                posit_2.push_back(num2);  // positron to metastable
                ss >> num2;
                alpha.push_back(num2);  // alpha emission
                ss >> num2;
                isomer.push_back(num2);  // isomeric transition
                // move to next line
                getline(infile1, line);
                ss.clear();
                ss.str(line);
                ss >> num1; // LI2 
                ss >> num2;
                spont_fiss.push_back(num2);  // spontaneous fission
                ss >> num2;
                delayed.push_back(num2);  // delayed neutron emission
                ss >> num2;
                decay_heat.push_back(num2);  // decay heat
                ss >> num2;
                nat_abund.push_back(num2);  // natural abund. %
                ss >> num2;
                air_rcg.push_back(num2);  // rad. con. guide - air
                ss >> num2;
                water_rcg.push_back(num2);  // rad. con. guide - water
                getline(infile1, line);
                ss.clear();
                ss.str(line);
                ss >> num1; // LI3
                ss >> num2;
                recover_gx.push_back(num2);  // recoverable energy per dis. g+x
                ss >> num2;
                beta_1.push_back(num2);  // beta decay to ground
                ss >> num2;
                beta_double.push_back(num2);  // double beta decay
                ss >> num2;
                neutron_decay.push_back(num2);  // neutron decay
                ss >> num2;
                beta_alpha.push_back(num2);  // beta + alpha decay
                
                // go back to beginning of file check if fission product
                infile1.clear();
                infile1.seekg(0, ios::beg);
                while (getline(infile1, line)){
                    if (line.length() > 10 && line.substr(0,4) == "   3"){
                    ss.clear();
                    ss.str(line);
                    ss >> num1;
                    ss >> word;
                       if(num1 == 3 && word != "DECAY"){
                          fiss_prod_pos++; 
                          if(num1 == 3 && atoi(word.c_str()) == nuclides[i]){
                            found_fiss_prod = true;
                            lib_type[i] = 3;
                            fission_prod_ind.push_back(i);
                          //  cout << "found fission product for " << nuclides[i] << endl;
                            lib_pos.push_back((fiss_prod_pos+2)/3);
                          }
                       } 
                    }
                }
                // mark position if not fission product
                // may go back and remove this later for space
                if(!found_fiss_prod){lib_pos.push_back(0);}
                fiss_prod_pos=0;
                found_fiss_prod = false;
                // leave loop if nuclide vector run through
                if((i+1) == nuclides.size()){break;}
                i++;
                // go back to beginning of file to search for next nuclide
                infile1.clear();
                infile1.seekg(0, ios::beg);
            }
            if(atoi(word.c_str()) != nuclides[i] ){
                exists_in_library = false;
            }

        }
    }
        // check to see if file was found in library
        if( not exists_in_library ){
            stringstream ss;
            ss << "ISOTOPE WAS NOT FOUND IN DECAY LIBRARY! CANNOT TRACK --> " << nuclides[i];
            string errorMessage = string(ss.str());
            throw logic_error(errorMessage);
        }

}

 /* 
 * This function reads in the initial list of nuclides for burnup tracking
 * and reads in the corresponding yield fractions from the Origen library
 * !!! Need to implement interpolation function for average fission energy !!! 
 */ 
void Library_builder::read_yield_lib(vector<int> nuclides, vector<double> Avg_FE){
    // open text files with decay data 
    // extracted from ORIGEN
    ifstream infile("origen.rev04.yields.data");   
    // dummy strings for reading in files
    string line;
    int num1;
    double num2;
    vector<double> temp_yield(3); // temp value to hold for interpolation
    bool fissile = false;
    bool fiss_prod_found = false;
    stringstream ss;
    int i=0; 
    
    // temporary vector to push into yield_frac array
    vector<double> buffer;
    int dim1=0;
    int dim2=0;
    
    // variables for interpreting yield library
    int num_FE; // number incident neutron energies yields are given
    
    // read in decay library files
    while ( i < nuclides.size()){
        // check to see if nuclide is fissile
        for(int j=0; j < fiss_array.size(); j++){
            if(nuclides[i] == fiss_array[j]){
                fissile = true;
                fission_nuc_ind.push_back(i);
                break;
            }
            else{fissile = false;}
        }
        if(fissile){
            yield_frac.push_back(buffer);
            dim2=0;
            for( int j = 0 ; j < nuclides.size(); j++){
                if(lib_type[j] == 3){
                // look for fissile nuclide place in file
                getline(infile, line); // read in first line, just comments
                while(getline(infile, line)){
                    if ( fiss_prod_found ){
                        fiss_prod_found = false;
                        break;
                    }
                    if (line.length() > 10){
                        ss.clear();
                        ss.str(line);
                        ss >> num2 >> num1; // read in first item
                        num1 = num2*pow(10.0,num1)+0.5; // trunctate to int for comparison
                        if(num1 == nuclides[i]){
                            cout << "found fissile nuclide in yield lib!" << endl;
                            ss.clear();
                            ss.str(line);
                            ss >> num2 >> num1 >> num2 >> num1 >> num_FE; // read in number of yield energies
                            // interpolate using average fission energy 
                            // determine what to do based on average FE and nuclide
                          for(int h = 0; h < num_FE; h++){
                            /* interpolation stuff added later */
                            getline(infile, line); // read next line of stuff, not nuclides
                            // look for fission yield fraction of nuclide[i] -> nuclide[j]
                            while (getline(infile, line)){
                                ss.clear();
                                ss.str(line);
                                ss >> num2 >> num1; // read in first nuclide
                                num1 = num2*pow(10.0,num1)+0.5;
                                if(num1 == nuclides[j]){
                                    cout << "found fission yield of " << nuclides[j] << " from fission of " << nuclides[i] << endl;
                                    cout << "number of fission energies = " << num_FE << endl;
                                    fiss_prod_found = true;
                                    ss >> num2 >> num1; // read in first nuclide
                                    num2 = num2*pow(10.0,num1);
                                    temp_yield[h] = num2;
                                   // yield_frac[dim1].push_back(num2);
                                   // dim2++;
                                    // go back to beginning of file to search for next nuclide
                                   // infile.clear();
                                   // infile.seekg(0, ios::beg);
                                    break;
                                }
                                ss >> num2 >> num1 >> num2 >> num1;; // read in first nuclide
                                num1 = num2*pow(10.0,num1)+0.5;
                                if(num1 == nuclides[j]){
                                    cout << "found fission yield of " << nuclides[j] << " from fission of " << nuclides[i] << endl;
                                    cout << "number of fission energies = " << num_FE << endl;
                                    fiss_prod_found = true;
                                    ss >> num2 >> num1; // read in first nuclide
                                    num2 = num2*pow(10.0,num1);
                                    temp_yield[h] = num2;
                                   // yield_frac[dim1].push_back(num2);
                                   // dim2++;
                                    // go back to beginning of file to search for next nuclide
                                   // infile.clear();
                                   // infile.seekg(0, ios::beg);
                                    break;
                                }
                                ss >> num2 >> num1 >> num2 >> num1;; // read in first nuclide
                                num1 = num2*pow(10.0,num1)+0.5;
                                if(num1 == nuclides[j]){
                                    cout << "found fission yield of " << nuclides[j] << " from fission of " << nuclides[i] << endl;
                                    cout << "number of fission energies = " << num_FE << endl;
                                    fiss_prod_found = true;
                                    ss >> num2 >> num1; // read in first nuclide
                                    num2 = num2*pow(10.0,num1);
                                    temp_yield[h] = num2;
                                   // yield_frac[dim1].push_back(num2);
                                   // dim2++;
                                    // go back to beginning of file to search for next nuclide
                                   // infile.clear();
                                   // infile.seekg(0, ios::beg);
                                    break;
                                }
                            }
                          }
                            // interpolate to get correct fission yield
                            num2 = interpolate_yields(temp_yield, num_FE, Avg_FE[i] );
                            yield_frac[dim1].push_back(num2);
                            dim2++;
                            // go back to beginning of file to search for next nuclide
                            infile.clear();
                            infile.seekg(0, ios::beg);
                        }
                        
                    }
                }
                }
            } 
        dim1++;
        }
        i++;
    }
   
/*    for(int j=0; j<dim1; j++){
        for(int k=0; k < dim2; k++){
        cout << "yield_frac[" << j << "][" << k << "] = " <<  yield_frac[j][k] << endl;
        }
    }
    
    
    for(int k=0; k < dim2; k++){
        cout << "fission_prod_ind " << k << " = " <<  fission_prod_ind[k] << endl;
    }
*/
    number_of_fiss = dim1;
    number_of_fp = dim2;
}

/* 
 * This function takes in fission yields at multiple energies
 * and interpolates linearly based on the average fission energy
 */ 
double Library_builder::interpolate_yields(vector<double> yields, int FE, double avg_FE){
    
    double temp;
    
    if( FE == 1 ){
        return yields[0];
    }
    if ( FE == 2 ){
        if( avg_FE <= fiss_energies[0] ){
            return yields[0];
        }
        if( avg_FE >= fiss_energies[1] ){
            return yields[1];
        }
        else{
            temp = (avg_FE-fiss_energies[0])/(fiss_energies[1] - fiss_energies[0]);
            temp = temp*(yields[1] - yields[0]);
            return temp+yields[0];
        }
    }
    
    if( FE == 3 ){
        if( avg_FE <= fiss_energies[0] ){
            return yields[0];
        }
        if( avg_FE >= fiss_energies[2] ){
            return yields[1];
        }
        else{
            if( avg_FE < fiss_energies[1] ){
                temp = (avg_FE-fiss_energies[0])/(fiss_energies[1] - fiss_energies[0]);
                temp = temp*(yields[1] - yields[0]);
                return temp+yields[0];
            }
             if( avg_FE >= fiss_energies[1] ){
                temp = (avg_FE-fiss_energies[1])/(fiss_energies[2] - fiss_energies[1]);
                temp = temp*(yields[2] - yields[1]);
                return temp+yields[1];
             }
        }
    }
}
       
