/* 
 * File:   Library_builder.h
 * Author: dlago_000
 *
 * Created on July 10, 2015, 2:32 PM
 */

#include <vector>
#include <string>

using namespace std;

#ifndef LIBRARY_BUILDER_H
#define	LIBRARY_BUILDER_H

// Library Class for Burnup Module //
/* This class contains methods for reading data files 
 * and for setting up the transition matrix.*/
class Library_builder {
public:
    // Constructor
    Library_builder();
    // Destructor
    virtual ~Library_builder();

    // get functions for variables
    // don't need set functions, variables set within class by library files
    // for vectors, functions take in index and return value
    
    int get_lib_type(int);

    int get_hl_units(int);
    
    double get_half_life(int);

    double get_beta_1(int);

    double get_beta_2(int);
    
    double get_posit_1(int);
    
    double get_posit_2(int);
    
    double get_alpha(int);
    
    double get_isomer(int);

    double get_spont_fiss(int);
    
    double get_delayed(int);
    
    double get_decay_heat(int);

    double get_recover_gx(int);
    
    double get_nat_abund(int);
    
    double get_water_rcg(int);
    
    double get_air_rcg(int);
    
    double get_beta_double(int);
    
    double get_neutron_decay(int);
    
    double get_beta_alpha(int);
    
    double get_lib_pos(int);
 
    double get_fiss_array(int);
    
    double get_fission_nuc_ind(int);

    double get_fission_prod_ind(int);

    double get_yield_frac(int, int);
    
    int get_number_of_fiss();

    int get_number_of_fp();
    
    
    // sort function to sort nuclides array for ease of use
    void sort(vector<int>&, vector<double>&, int);
    
    // function to read in library files
    // also takes in concentrations to be sorted with nuclide IDs
    // need to change to read in vector of fission energies
    void read_library_files(vector<int>&, vector<double>&, vector<double>);
    // function to read in decay data
    void read_decay_lib(vector<int>);
    // function to read in fission yield data
    void read_yield_lib(vector<int>, vector<double>);
    
    // function to interpolate between fission yield energies 
    double interpolate_yields(vector<double>, int, double);
    
    
protected:
/* data structures for constructing transition matrix*/    
    
    // declare vectors for reading in data, to be used to 
    // construct transition matrix
    // library type: 
    // 1 = activation products
    // 2 = actinides
    // 3 = fission products
    vector<int> lib_type;
    
    // units of half life
    // 1 = seconds
    // 2 = minutes
    // 3 = hours
    // 4 = days
    // 5 = years
    // 6 = stable
    // 7 = 1E3 years
    // 8 = 1E6 years
    // 9 = 1E9 years
    vector<int> hl_units;
    
    // half-life
    vector<double> half_life;
    
    /* Branching Fractions */
    
    // beta decay to a daughter in ground state
    vector<double> beta_1;
    
    // beta decay to a daughter in metastable state
    vector<double> beta_2;
    
    // positron emission/electron capture to ground state
    vector<double> posit_1;
    
    // positron emission/electron capture to metastable state
    vector<double> posit_2;
    
    // alpha particle emission
    vector<double> alpha;
    
    // isomeric transition
    vector<double> isomer;
    
    // LI2, LI3 for formatting only 
    
    // spontaneous fission
    vector<double> spont_fiss;
    
    // delayed neutron decay (neutron and beta particle emission)
    vector<double> delayed;
    
    // decay heat, multiple values (not utilized currently)
    vector<double> decay_heat;
    
    // fraction of recoverable energy per disintegration from gamma+x-rays 
    // (not utilized current)
    vector<double> recover_gx;
    
    // atom percent abundance of naturally occurring isotopes ABUND(I)
    vector<double> nat_abund;
    
    //  radioactivity concentration guides (not utilized currently)
    vector<double> water_rcg;
    vector<double> air_rcg;
    
    // double beta decay
    vector<double> beta_double;
    
    // neutron decay
    vector<double> neutron_decay;
    
    // beta decay plus an alpha particle
    vector<double> beta_alpha;
    
    // placeholder for position in fission yield library
    // fission products are in same order in decay library
    // currently, 1151 fission products in decay lib
    vector<int> lib_pos;
 
    // vector of fissionable isotopes that have yields
    // currently only 30 fissionable isotopes in library
    vector<int> fiss_array;
    
    // vector of fission energies boundaries in eV for fission yields
    // 1. Thermal - 2.53000E-02 (0.0253 eV)
    // 2. Epithermal - 2.00000E+06 (2.0 MeV)
    // 3. Fast - 1.40000E+07 (14.0 MeV)
    vector<double> fiss_energies;
    
    // vector listing the positions of the fissionable isotopes in nuclide array
    vector<int> fission_nuc_ind;
    // vector listing the positions of fission products in nuclide array
    vector<int> fission_prod_ind;
    // 2-d vector listing all the fission products and fission yields for each isotope
    // yield_frac[x][y] --> frac for nuclide y from x
    vector < vector<double> > yield_frac;
    
    // number of fissionable nuclides in array
    int number_of_fiss;
    // number of fission products in array
    int number_of_fp;


};

#endif	/* LIBRARY_BUILDER_H */

