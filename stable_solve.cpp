/* 
 * File:   stable_solve.cpp
 * Author: Daniel
 * 
 * Created on February 2, 2016, 12:58 PM
 */

#include "stable_solve.h"
#include<cstdlib>
#include<iostream>

stable_solve::stable_solve() {
    
}

stable_solve::~stable_solve() {
    
}

/*
 * This function solves for the nuclide concentration if
 * a flux is present, using the generalized form of the 
 * batch Bateman equations given a linear chain. 
 * 
 */
void stable_solve::batch_solve1(depletion* decay, tran_mat* matrix, vector<double>& conc_s){
    
};

/*
 * This function solves for the nuclide concentration if
 * NO flux is present, using the generalized form of the 
 * batch Bateman equations given a linear chain. 
 * 
 */ 
void stable_solve::batch_solve2(depletion* decay, tran_mat* matrix, vector<double>& conc_s){
    
    /*
     * Assuming no flux, the following Bateman equation can be used to solve
     * for any nuclide Ni:
     * Ni(t) = (N1(0)*lambda_1* ... *lambda_i-1 )*SUM[lambda_j*alpha_j*exp(-lambda_j*t)]
     * where alpha_j = PRODUCT_SUM[1/(lambda_k-lambda_j)]
     * 
     * Consequently, the chain for each nuclide must be enumerated.
     *  
     */ 
    
    bool done = false; // logical to check if final precursor found
    bool prodsum = false;
    double br; // branching ratio holder
    int n_i, n_j, index;
    double lam1, No, sum1=0, sum2=0, sum3=0;
    double n_s0;
    double time = decay->get_stable_time();
    vector<double> lams;
    
    for(int i=0; i < conc_s.size(); i++){
        // for each nuclide, work backwards to find what the preceding element 
        // in the chain is, looping until the initial nuclide is found
        // set first nuclide in chain
        n_i = matrix->stable[i];
        n_s0 = matrix->get_original_conc(n_i);
        do{
            // loop through entire nuclide list to see if precursor found
            for(int j=0;j<matrix->nuclides_full.size(); j++){
                // work backwards to see if n_i came from n_j
                n_j = matrix->nuclides_full[j];
//                cout << "n_i = " << n_i << endl;
//                cout << "n_j = " << n_j << endl;
                
                br = matrix->branching_ratio_calculator(decay->library, n_j , n_i, j);
//                cout << br << endl;
                if(br > fabs(1E-14)){
                    lam1 = decay->library->get_half_life(j);
                    lam1 = matrix->convert_to_lambda(lam1, decay->library->get_hl_units(j));
//                    cout << "found precursor of " << n_i << " to be " << n_j << endl; 
//                    cout << "value of lambda is = " << lam1 << endl;
                    lams.push_back(lam1);
                    // precursor to n_i and search for it's precursor in the chain
                    n_i = n_j;
                    break; 
                }
                if(j == matrix->nuclides_full.size()-1){
//                    cout << "initial nuclide in chain is " << n_i << endl;
                    for(int k=0; k<matrix->unstable.size(); k++){
//                        cout << matrix->unstable[k] << " -> " << decay->get_initial_conc_u(k) << endl;
                        if(n_i == matrix->unstable[k]){
                            // No = decay->get_initial_conc_u(k);
                            No = matrix->get_original_conc(n_i);
                        }
                    } 
                    done = true;
                }
            } 
        }while(not done);
        
        // add final lambda for stable isotope
        lams.push_back(0.0);
        
        // using vector of decay probs, calculate final concentration of
        // stable isotope
        for(int j=0; j<lams.size(); j++){
            for(int k=0; k<lams.size(); k++){
                if(k != j){
                    if(prodsum){
                        sum2 = sum2*(lams[k]-lams[j]);
//                        cout << "diff = " << lams[k] - lams[j] << endl;
//                        cout << "next sum2 =  " << sum2 << endl;
                    }
                    if(not prodsum){
                        sum2 = (lams[k]-lams[j]);
//                        cout << "initial sum2 = " << sum2 << endl;
                        prodsum = true;
                    }

                }
            }
//            cout << "sum2 = " << sum2 << endl;
            prodsum = false;
            if(j == 0){
                sum1 = lams[j];
                sum3 = exp(-lams[j]*time)/sum2;
//                cout << "sum3 = " << sum3 << endl;
            }
            if(j > 0){
                sum3 = sum3+exp(-lams[j]*time)/sum2;
//                cout << "sum3 = " << sum3 << endl;
                if(j != lams.size()-1){
                    sum1 = sum1*lams[j];
                }
            }
            
            sum2 = 0;
        }
        
        // get initial conc of stable isotope
        
        conc_s[i] = No*sum1*sum3+n_s0;
        lams.erase(lams.begin(),lams.begin()+lams.size());
        sum1 =0; sum2=0; sum3=0;
//        cout << "final conc of " << matrix->stable[i] << " = " << conc_s[i] << endl; 
    }
     
};