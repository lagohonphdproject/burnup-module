/* 
 * File:   output_proc.h
 * Author: Daniel
 *
 * Created on February 11, 2016, 1:02 PM
 * 
 * This class handles the data from the burnup calculation and provides
 * all the solutions - concentrations for each nuclide at each timestep
 * 
 */

#include <vector>
#include <string>
#include <cstdlib>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include "math.h"
#include "stdio.h"


using namespace std;

#ifndef OUTPUT_PROC_H
#define	OUTPUT_PROC_H

class output_proc {
public:
    
    // constructor
    output_proc();
    
    // destructor
    virtual ~output_proc();
    
  
    void print_all(vector<int>, vector<double>, vector< vector<double> >);
    
    
private:

    
    
};

#endif	/* OUTPUT_PROC_H */

