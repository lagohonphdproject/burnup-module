/* 
 * File:   cram.cpp
 * Author: Daniel
 * 
 * Created on August 21, 2015, 1:31 PM
 */
using namespace std;

#include "cram.h"
#include<cstdlib>
#include<iostream>


cram::cram() {
}

cram::~cram() {
    buffer1.clear();
    buffer2.clear();
    conc_f.clear();
    theta_real.clear();
    theta_cmplx.clear();
    alpha_real.clear();
    alpha_cmplx.clear();
//    buffer3.clear();
    
}


void cram::cram_solver_1(depletion* decay_param, tran_mat* A, int k) {
    /* cram solver inputs
     * theta = poles of rational function r
     * alpha = residues at poles
     * alpha_0 = limit as r-> inf
     * A = transition matrix, ordered ascending by mass number
     * t = time step
     * n_0 = initial concentration
     */
    
    // generate coefficients
    // call function "quadrature coefficients"
    // generates theta, alpha, alpha_0
    quadrature_coefficients(k);
    
    /* PSEUDOCODE
     * s = length(theta)
     * A=A*t;
     * 
     * for j = 1 to s
     *  n = n + (A - theta_j * I)/(alpha_j * n_0)
     * end
     * n = 2* real(n)
     * n = n + alpha_0 * n_0
     */
    
    // temporary vector for sparse solve
    vector<double> temp_vec;
    vector<double> temp_conc_i;
    
    int s;
    // multiply transition matrix by time_step
    s = alpha_real.size();
    
    // multiply transition matrix by time_step
    // diag then off-diag
    double time = decay_param->get_time();

    for(int i = 0; i < A->diag.size(); i++ ){
        buffer1.push_back(A->diag[i]*time);
        conc_f.push_back(0.0);
        temp_vec.push_back(0.0);
        temp_conc_i.push_back(decay_param->get_initial_conc_u(i));
     
    }
    for(int i = 0; i < A->off_diag_1.size(); i++ ){
    //    if(not A->reactions){
         
           buffer2.push_back(A->off_diag_1[i]*time); 
    }

    
    
    // loop through and solve using gaussian elimination
    // n = n + (A - theta_j * I)/(alpha_j * n_0)
    
    for(int j =0; j < s; j++){
        // call sparse solve
//        sparse_solve_GS(temp_vec, A, buffer1, buffer2, temp_conc_i, j );
        LU_solve(temp_vec, A, buffer1, buffer2, temp_conc_i, j);
             for(int i =0; i<A->diag.size(); i++){
                conc_f[i] += temp_vec[i];
                temp_vec[i] = 0.0;
            } 
    }

    for(int i=0; i<A->diag.size(); i++){
        conc_f[i] = 2*conc_f[i] + temp_conc_i[i]*alpha_0;
        if(conc_f[i] < 0){
            cout << "SETTING TO ZERO!!!" << endl;
            cout << "conc_f[" << i << "] = " << conc_f[i] << endl;
            conc_f[i] = 0.0;
        }
    }
    
    for(int i =0; i<temp_vec.size(); i++){
        cout << "conc_f[" << A->nuclides_full[i] << "] = " << scientific << setprecision(15) << conc_f[i] << endl;
    }
    
    // set final concentration in object for return
    decay_param->set_final_conc_u(conc_f);
    
//    cout << "size of temp_vec = " << temp_vec.size() << endl;
//    decay_param->set_final_conc_u(temp_vec);
//    

   
 
    
}

void cram::quadrature_coefficients(int k) {
    
    // calculate pi
    const double pi  = 3.1415926535897932384626433832795;
    
    // generate coefficients
    // call function "quadrature coefficients"
    // generates theta, alpha, alpha_0
    
    /* PSEUDOCODE
     * x = pi * (1:2:k-1)/k;
     * theta = k * (0.1309 - 0.1194 * x^2 + 0.2500x * 1i);
     * w_j = k * ( -2 * 0.1194 * x + 0.2500 * 1i);
     * alpha = 1i * 1/k * exp(theta) .* w_j;
     * alpha_0 = 0;
     */
    vector<double> temp;
    int iter;
    for(int i = 0; i < (k-1); i+=2){
        temp.push_back(pi*(i+1)/k);
        iter++;
    }
    
    // temporary vector to calculate alpha
    vector<double> w_j_real, w_j_cmplx;
    
    // solve for theta and alpha, complex coefficients
    for(int i=0; i<temp.size(); i++){
        // real component of theta
        theta_real.push_back(k*(0.1309- 0.1194 * temp[i]*temp[i]));
        // complex component of theta
        theta_cmplx.push_back(k*(0.25*temp[i]));
      //  cout << theta_real[i] << " + " << theta_cmplx[i] << "i" << endl;
        // w_j
        w_j_real.push_back(k * ( -2 * 0.1194 * temp[i]));
        w_j_cmplx.push_back(k*(0.25));        
        // real component of alpha
        // For complex Z=X+i*Y, exp(Z) = exp(X)*(COS(Y)+i*SIN(Y))
        alpha_real.push_back(-1*(w_j_cmplx[i]*exp(theta_real[i])*cos(theta_cmplx[i])+w_j_real[i]*exp(theta_real[i])*sin(theta_cmplx[i]))/k);
        // complex component of alpha
        alpha_cmplx.push_back(1*(w_j_real[i]*exp(theta_real[i])*cos(theta_cmplx[i])-w_j_cmplx[i]*exp(theta_real[i])*sin(theta_cmplx[i]))/k);
        // cout << alpha_real[i] << " + " << alpha_cmplx[i] << "i" << endl;
    }
    
    // for now, alpha_0 = 0
    alpha_0 = 0.0;
    
}

void cram::sparse_solve_GS(vector<double>& x, tran_mat* A, vector<double> diag, vector<double> offd, vector<double> b, int ind){
    
    // implement Gauss-Seidel to numerically solve for x
    vector<double> temp1, temp2;
    double sum, dom_check, error, error_new;
    int err_it = 0;
    
    // check to see if matrix is strictly diagonally dominant
    // initialize vectors with zeros for work later
    for(int i=0; i<diag.size(); i++){
        sum=0;
        temp1.push_back(0);
        temp2.push_back(0);
        // sum off-diagonals in row i
        for(int j=0; j<offd.size(); j++){
            if(A->row_ind_1[j] == i){
                sum += fabs(offd[j]);
            }
        }
        dom_check = fabs(diag[i])*sqrt(pow(theta_real[ind],2)+pow(theta_cmplx[ind],2)) - sum;
//        cout << "dom_check == " << dom_check << endl;
//        cout << "sum = " << sum << endl;
        if(dom_check < 0){
            cout << "The matrix is not strictly diagonally dominant in row " << i << " !!!" << endl;
        }
    }
    sum = 0;

    int iter = 0;
    do{
        error = 0;
        error_new = 0;
        sum = 0;
        iter++;
        for(int i=0; i<diag.size(); i++){
            
            // sum up product of row i with solution vector (A_i * temp1)
            for(int j=0; j<offd.size(); j++){
               // cout << "row ind = " << A->row_ind_1[j] << endl;
                if(A->row_ind_1[j] == i){
                    sum = sum+(offd[j]*temp1[A->col_ind_1[j]]);
//                    cout << "In iter = " << iter << " sum[" << i << "] == " << sum << endl;
                }
            }
            // calculate new value of x[i]
            x[i] = ((alpha_real[ind]*diag[i]-theta_real[ind]*alpha_real[ind]-theta_cmplx[ind]*alpha_cmplx[ind])*b[i]- 
            (diag[i]-theta_real[ind])*sum)/(pow((diag[i]-theta_real[ind]),2)+pow(theta_cmplx[ind],2));
//            // calculate new error quantity
            error_new = sqrt(pow((x[i] - temp1[i]),2));           
            error = max(error,error_new);
            err_it ++;
//          err  
//            cout << "error = " << error << endl;
            sum = 0;
            temp1[i] = x[i]; // save previous solution to calculate error
        }
    }while(error > 1e-6);
    

    
    // all below was attempt to implement direct solver
    
    /*
    
    /*
     * Solve the system Ax = b where b is ni, the initial nuclide concentration
     * this function is specific to the CRAM method:
     * A = A - theta*I
     * ni = ni*alpha
     * or
     * (A - theta_j * I)/(alpha_j * n_0)
     
    // temporary variable for reducing 
    double temp_ratio;
    
    cout << "ni[0] = " << ni[0] << endl;
    // first reduce down
    for(int i=0; i < diag.size(); i++){
        ni[i] = ni[i]*alpha;
        for(int j=i; j < A->col_ind_1.size(); j++){
            if( A->col_ind_1[j] == i){
                // calculate ratio to multiply row with for subtraction
                temp_ratio = (offd[j]-theta)/(diag[i]-theta);
                // zero out element under element (i,i)
                offd[j] = 0;
                // operate on rest of row where element was reduced to zero
                for(int k = j+1; k< A->row_ind_1.size(); k++){
                    if(A->row_ind_1[k] == A->row_ind_1[j]){
                        // see if there is corresponding element in row i
                        int iter=0;
                        do{
                            if(A->row_ind_1[iter] == i and A->col_ind_1[iter] == A->col_ind_1[k]){
                                offd[k] = offd[k] - temp_ratio*(offd[iter]-theta);
                                ni[k] = ni[k] - temp_ratio*(offd[iter]-theta);
                                if(ni[k] < 0){ ni[k] = 0;}   
                                // check to see if adding a new element to row
                                
                            }
                            iter++;
                        }while(A->row_ind_1[iter] < i+1);
                    }
                }
            }
        }
    }
    cout << "ni[0] = " << ni[0] << endl;
    
    // start from the bottom, reduce up so that result is diagonal matrix
    for(int i = 0; i < offd.size(); i++){
        // loop through off-diagonals and zero them out
        if(fabs(offd[i]) > 1E-32){
            temp_ratio = (offd[i]-theta)/(diag[A->col_ind_1[i]]-theta);
            offd[i] = offd[i] - temp_ratio*(offd[i]-theta);
            ni[i] = ni[i] - temp_ratio*(offd[i]-theta);
            if(ni[i] < 0){ ni[i] = 0;}
        }
    }
    
    for(int i=0; i<diag.size(); i++){
        ni[i] = ni[i]/diag[i];
        x[i] = ni[i];
        cout << "ni[" << i << "] = " << ni[i] << endl;
    }
    
    cout << "printing out matrix after reducing" << endl;
    for(int i=0; i < offd.size(); i++){
        offd[i] = offd[i]/diag[A->row_ind_1[i]] ;
       
        cout << "offd[" << i << "] = " << offd[i] << endl;
    }
    
    cout << "size of x = " << x.size() << endl;
    
     * 
     * 
     */
    
}

void cram::LU_solve(vector<double>& x, tran_mat* A, vector<double> diag, vector<double> offd, vector<double> b, int ind){
    
    // declare LU vector holders
    // complex and real placeholders to carry through complex analysis
    vector<double> Lr, Lc , Ur, Uc;
    vector<double> temp_vec;
    vector<int> L_row_ind , L_col_ind, U_row_ind, U_col_ind;
    int iter=0;
    int index, diag_ind;
    double temp, temp1, temp2, value, value1, value2, value3, value4, denom;
    bool first_pass = true;
    bool assigned = false;
    bool assigned1 = false;
    bool assigned2 = false;
    bool assigned3 = false;
    bool assigned4 = false;
    
    vector<double> temp_d, temp_c;// temporary vectors for finding U matrix real and complex
    vector<int> temp_r, temp_i;
    
    // temporary "A" matrix for LU calculation
    for(int i=0; i<diag.size()+offd.size(); i++){
        if(i < diag.size()){
            temp_d.push_back(diag[i]-theta_real[ind]);
            temp_c.push_back(0.0-theta_cmplx[ind]);
            temp_r.push_back(i);
            temp_i.push_back(i);
        }
        else{
            temp_d.push_back(offd[i-diag.size()]);
            temp_c.push_back(0.0);
            temp_r.push_back(A->row_ind_1[i-diag.size()]);
            temp_i.push_back(A->col_ind_1[i-diag.size()]);
        } 
   // cout << "temp_d[" << temp_r[i]+1 << "," << temp_i[i]+1 << "] = " << temp_d[i] << endl;    
    }

    double eps = numeric_limits<double>::epsilon();
    double test_value, test_value_c;
    // first, find LU decomposition
    // do this without any row exchanges
    // put first element in U matrix
    Ur.push_back(temp_d[0]);
    Uc.push_back(0.0);
    U_row_ind.push_back(0);
    U_col_ind.push_back(0);
    
    for(int i=0; i<diag.size(); i++){
        if(fabs(diag[i]-theta_real[ind]) < sqrt(eps) ){ 
            cout << "Small pivot encountered in column " << i << endl;
        }
        Lr.push_back(1);
        Lc.push_back(0.0);
        L_row_ind.push_back(i);
        L_col_ind.push_back(i);

        iter++;
        for(int k = i+1; k<diag.size(); k++){
            for(int j=0; j<temp_d.size(); j++){
                if(temp_r[j]==k and temp_i[j]==i){
                    // temp = diag[i] - theta_real[ind];
                    for(int g=0; g<temp_d.size(); g++){
                        if(temp_r[g]==i and temp_i[g]==i){
                            diag_ind = g;                            
                        }
                    }
                    temp1 = temp_d[diag_ind];
                    temp2 = temp_c[diag_ind];
                    denom = (pow(temp1,2)+pow(temp2,2));
                    Lr.push_back((temp_d[j]*temp1+temp_c[j]*temp2)/denom);
                    Lc.push_back((temp_c[j]*temp1-temp_d[j]*temp2)/denom);
                    assigned = false;
                    L_row_ind.push_back(k);
                    L_col_ind.push_back(i);
                   // cout << "L(" << k << "," << i << ") = " << (temp_d[j]*temp1+temp_c[j]*temp2)/denom << " + " << (temp_c[j]*temp1-temp_d[j]*temp2)/denom << "i" << endl; 
                    iter++;
                }          
            }
            for(int h = i+1; h<diag.size(); h++){ // cycle through rest of row k
                for(int g=0; g<temp_d.size(); g++){
                    if(temp_r[g]==k and temp_i[g]==h){
                        first_pass=false;
                        index = g;
                    }
                }
                //if(first_pass){
                    for(int j=0; j<Lr.size(); j++){ // L[k,i]
                        if(L_row_ind[j]==k and L_col_ind[j]==i){
                            value1 = Lr[j];
                            value2 = Lc[j];
                            assigned1 = true;
                        }
                        if(not assigned1){
                            value1=0; value2=0;
                        }
                    }
                    assigned1 = false;
                    for(int g=0; g<temp_d.size(); g++){ // A[i,h]
                        if(temp_r[g]== i and temp_i[g]==h){
                            value3 = temp_d[g];
                            value4 = temp_c[g];
                            assigned2 = true;
                        }
                        if(not assigned2){
                            value3=0.0;
                            value4=0.0;
                        }
                    }
                    assigned2 = false;
                    
                    if(first_pass){
                        test_value = value1*value3-value2*value4;
                        test_value_c = value1*value4+value2*value3;
                        if(fabs(test_value)>1E-30 or fabs(test_value_c)>1E-30){
                            temp_d.push_back(-1*(test_value));
                            temp_c.push_back(-1*(test_value_c));
                            temp_r.push_back(k);
                            temp_i.push_back(h);
                            assigned3 = true;
                        }
                    }
                    
                    if(not first_pass){
                        temp_d[index] = (temp_d[index]-(value1*value3-value2*value4));
                        temp_c[index] = (temp_c[index]-(value1*value4+value2*value3));
                    }   
                assigned3 = false;
                first_pass = true;;
            }
        }
        for(int j=i; j<diag.size(); j++){   
            for(int h=0; h<temp_d.size(); h++){
                if(temp_r[h] == i and temp_i[h] == j){
                    for(int g=0; g<Ur.size(); g++){
                        if(U_row_ind[g] == i and U_col_ind[g]==j){
                            Ur[g] = temp_d[h];
                            Uc[g] = temp_c[h];
                            assigned4 = true;
                        }
                    }
                    if(not assigned4){
                        Ur.push_back(temp_d[h]);
                        Uc.push_back(temp_c[h]);
                        U_row_ind.push_back(temp_r[h]);
                        U_col_ind.push_back(temp_i[h]);
                    }
                }
                assigned4 = false;
            }
        }
    }
    

    for(int i=0; i<Ur.size(); i++){
        if(U_row_ind[i] > U_col_ind[i]){
        //    cout << "U(" << U_row_ind[i]+1 << "," << U_col_ind[i]+1 << ") = " << Ur[i] << " + " << Uc[i] << "i" << endl;
            Ur.erase(Ur.begin()+i);
            Uc.erase(Uc.begin()+i);
            U_row_ind.erase(U_row_ind.begin()+i);
            U_col_ind.erase(U_col_ind.begin()+i);
            i--;
        }
        else{
        //    cout << "U(" << U_row_ind[i]+1 << "," << U_col_ind[i]+1 << ") = " << scientific << setprecision(15) << Ur[i] << " + " << Uc[i] << "i" << endl;
        }  
    }
    cout << "SIZE OF U = " << Ur.size() << endl;
    cout << "SIZE OF L = " << Lr.size() << endl;
//    for(int i=0; i<Lr.size(); i++){
//        cout << "L3(" << L_row_ind[i]+1 << "," << L_col_ind[i]+1 << ") = " << scientific << setprecision(15) << Lr[i] << " + " << Lc[i] << "i" << endl;
//    }
//    cout << "SIZE OF U = " << Ur.size() << endl;
//    for(int i=0; i<Ur.size(); i++){
//        cout << "U2(" << U_row_ind[i]+1 << "," << U_col_ind[i]+1 << ") = " << Ur[i] << " + " << Uc[i] << "i;" << endl;
//    }
    
    // With L and U matrices constructed, now solve the following systems:
    // y=L\(alpha(j) * Ni);
    // x = U\y;
    // Nf = Nf + x;
    
    vector<double> Ni_real, Ni_cmplx;
    vector<double> yr(diag.size(),0.0), yc(diag.size(),0.0);
    vector<double> xr(diag.size(),0.0), xc(diag.size(),0.0);
    vector<double> temp_hold(diag.size(),0);
    int row_ctr=0;
    int dgnl;
    int i1,i2;
   
    // b is initial concentration vector
//    for(int i=0; i<diag.size(); i++){
//    //    cout << "conc[" << i << "] = " << b[i] << endl;
//        Ni_real.push_back(alpha_real[ind]*b[i]);
//        Ni_cmplx.push_back(alpha_cmplx[ind]*b[i]);
//    }
    
    temp= 0.0;
    value=0.0;
    value1 = 0.0;
    value2 = 0.0;
    value3 =0.0;
    
    // solve for y --> y = L/Ni
    // note: Ni = Ni*alpha + NI*alpha*i
    // know first value
    
    denom = pow(Lr[0],2)+pow(Lc[0],2);
    yr[0] = ((alpha_real[ind]*Lr[0]+alpha_cmplx[ind]*Lc[0])*b[0])/(denom);
    yc[0] = ((alpha_cmplx[ind]*Lr[0]-alpha_real[ind]*Lc[0])*b[0])/(denom);
    
    for(int i=1; i<yr.size(); i++){
        //start from top row
        for(int j=1; j<Lr.size(); j++){
            // see how many are in current row for reduction
            if(L_row_ind[j] == i){
                row_ctr++;
                temp_hold[row_ctr-1] = j;
            }
        }
        for(int j=0; j<row_ctr; j++){
            if(L_col_ind[temp_hold[j]] == L_row_ind[temp_hold[j]]){
                // diagonal value of L, use in denominator
                temp = 1/(pow(Lr[temp_hold[j]],2)+pow(Lc[temp_hold[j]],2));
                dgnl = temp_hold[j];
            }
            i1 = temp_hold[j];
            i2 = L_col_ind[temp_hold[j]];
            value = value + (Lr[i1]*yr[i2]-Lc[i1]*yc[i2]); // real
            value1 = value1 + (Lr[i1]*yc[i2]+Lc[i1]*yr[i2]); // complex
        }
        //value2 = b[i] * alpha_
        // DIAGONAL OF L == 1 +0 i ALWAYS!;
        yr[i] = temp*(b[i]*alpha_real[ind]*Lr[dgnl]-value*Lr[dgnl]-value1*Lc[dgnl]-b[i]*alpha_real[ind]*Lc[dgnl]);
    //    yr[i] = temp*(b[i]*alpha_real[ind]-value);
        yc[i] = temp*(b[i]*alpha_cmplx[ind]*Lr[dgnl]+value*Lc[dgnl]-b[i]*alpha_cmplx[i]*Lc[dgnl]-value1*Lr[dgnl]);
    //    yc[i] = temp*(b[i]*alpha_cmplx[ind]-value1);
        row_ctr = 0;
        temp =0.0;
        value =0.0;
        value1=0.0;
    }

    
//    for(int i=0; i<yr.size(); i++){
////        cout << "b[" << i << "] = " << b[i] << endl;
//        cout << "y[" << i << "] = " << yr[i] << " + " << yc[i] << "" << endl;
//    }
    
    // with y, now solve x = U\y
    // start from the bottom (now we here)
    for(int i=0; i<Ur.size(); i++){
        if(U_row_ind[i] == diag.size()-1 and U_col_ind[i] == diag.size()-1){
            dgnl = i;
        }
    }
    
  //  cout << "U(12,12) = " << Ur[dgnl] << " + " << Uc[dgnl] << "i" << endl;
    
    xr[xr.size()-1]=(yr[yr.size()-1]*Ur[dgnl]+Uc[dgnl]*yc[yc.size()-1])/((pow(Ur[dgnl],2)+pow(Uc[dgnl],2)));
    xc[xc.size()-1]=(yc[yc.size()-1]*Ur[dgnl]-Uc[dgnl]*yr[yr.size()-1])/((pow(Ur[dgnl],2)+pow(Uc[dgnl],2)));
    
    row_ctr = 0;
    temp= 0.0;
    value=0.0;
    value1 = 0.0;
    value2 = 0.0;
    value3 =0.0;
    dgnl =0;

    for(int i=xr.size()-2; i>=0; i--){
        //accumulate row sum
        for(int j=Ur.size()-1; j>=0; j--){
            // see how many are in current row for reduction
            if(U_row_ind[j] == i){
                row_ctr++;
                temp_hold[row_ctr-1] = j;
            }
        }
        for(int j=0; j<row_ctr; j++){
            if(U_col_ind[temp_hold[j]] == i and U_row_ind[temp_hold[j]] == i){
                // diagonal value of L, use in denominator
                dgnl = temp_hold[j];
                temp = 1/(pow(Ur[dgnl],2)+pow(Uc[dgnl],2));
            }
            if(U_col_ind[temp_hold[j]] != i and U_row_ind[temp_hold[j]] == i){
                i1 = temp_hold[j];
                i2 = U_col_ind[temp_hold[j]];
                value = value + (Ur[i1]*xr[i2]-Uc[i1]*xc[i2]); // real
                value1 = value1 + (Ur[i1]*xc[i2]+Uc[i1]*xr[i2]); // complex
            }
        }
        xr[i] = temp*((yr[i]-value)*Ur[dgnl]+(yc[i]-value1)*Uc[dgnl]);
        xc[i] = temp*((yc[i]-value1)*Ur[dgnl]-(yr[i]-value)*Uc[dgnl]);
//        xr[i] = temp*(yr[i]*Ur[dgnl]-value*Ur[dgnl]+yc[i]*Uc[dgnl]-value1*Uc[dgnl]);
//        xc[i] = temp*(value*Uc[dgnl]-yr[i]*Uc[dgnl]+yc[i]*Ur[dgnl]-value1*Ur[dgnl]);
        row_ctr = 0;
        temp =0.0;
        value =0.0;
        value1=0.0;
        temp_hold.clear();
    }
    
        for(int i=0; i<xr.size(); i++){
    //    cout << "b[" << i << "] = " << b[i] << endl;
    //    cout << "x[" << i+1 << "] = " << scientific << setprecision(15) << xr[i] << " + " << xc[i] << "i" << endl;
          // assigned solution to return x as answer
            x[i] = xr[i];
        }
    
    
}





