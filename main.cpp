/* 
 * File:   main.cpp
 * Author: dlago_000
 *
 * Created on July 8, 2014, 6:52 PM
 */



#include <cstdlib>
#include <iostream>
#include <vector>
#include "tran_mat.h"
#include "library_builder.h"
#include "depletion.h"
#include "output_proc.h"


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "APIDA - API for Depletion Analysis\n" ;
    cout << "\n";
    cout << "Author: Daniel Lago, 2016\n";
    
    // call input reader for standalone option
    
    // instantiate classes for in-memory?
    
    // NEEDED FROM RYAN
    // 1. Reaction rates (flux*xsec)
    // 2. Average fission energy per nuclide (for interpolating fiss yield)
    // 3. Cross sections for each reaction type?
    
    // call library class to populate transition matrix
        // depletion->tmatrix(nuclides, rxn_rates) 
    vector<int> dummy;
    vector<int> temp_hold_dummy;
    

    // initial concentrations
    // also feed into library function to be sorted with nuclides IDs
    vector<double> conc_i; // initial concentrations    
    vector<double> time;   // burnup time steps
    
// ---------------------------------------------------------------------------
// Defining nuclides, concentrations, rxns, and times
    
    dummy.push_back(922350); // u235
    dummy.push_back(922360); // u236
    dummy.push_back(922370); // u237
    dummy.push_back(922380); // u238
    dummy.push_back(922390); // u239
    dummy.push_back(932360); // np236
    dummy.push_back(932370); // np237
    dummy.push_back(932380); // np238
    dummy.push_back(932390); // np239
    dummy.push_back(942360); // pu236
    dummy.push_back(942380); // pu238
    dummy.push_back(942390); // pu239
    dummy.push_back(942400); // pu240
    dummy.push_back(942410); // pu241
    dummy.push_back(942420); // pu242
    dummy.push_back(942430); // pu243
    dummy.push_back(952410); // am241
    dummy.push_back(952420); // am242
    dummy.push_back(952430); // am243
    dummy.push_back(952440); // am244
    dummy.push_back(962420); // cm242
    dummy.push_back(962430); // cm243
    dummy.push_back(962440); // cm244
    
    conc_i.push_back(1E10); // u235
    conc_i.push_back(0); // u236
    conc_i.push_back(0); // u237
    conc_i.push_back(1E10); // u238
    conc_i.push_back(0); // u239
    conc_i.push_back(0); // np236
    conc_i.push_back(0); // np237
    conc_i.push_back(0); // np238
    conc_i.push_back(0); // np239
    conc_i.push_back(0); // pu236
    conc_i.push_back(0); // pu238
    conc_i.push_back(0); // pu239
    conc_i.push_back(0); // pu240
    conc_i.push_back(0); // pu241
    conc_i.push_back(0); // pu242
    conc_i.push_back(0); // pu243
    conc_i.push_back(0); // am241
    conc_i.push_back(0); // am242
    conc_i.push_back(0); // am243
    conc_i.push_back(0); // am244
    conc_i.push_back(0); // cm242
    conc_i.push_back(0); // cm243
    conc_i.push_back(0); // cm244

    
    // 2d vector holding rxn_rates of all elements
    vector< vector<double> > rxns;
    vector<double> row_hold(10, 0.0); // 6 types of rxns
   /* column neutron-nuclide interactions (n_i to n_j)
    * 1. (n,gamma) (+000010)
    * 2. (n,2n)    (-000010)
    * 3. (n,3n)    (-000020)
    * 4. (n,4n)    (-000030)
    * 5. (n,p)     (-010000)
    * 6. (n,d)     (-010010)
    * 7. (n,t)     (-010020)
    * 8. (n,He-3)  (-020020)
    * 9. (n,alpha) (-020030)
    * 10. Fission
    */
    
    row_hold[0] = 1E-5;
    rxns.push_back(row_hold); // u235 n,gamma
    rxns.push_back(row_hold); // u236 n,gamma
    row_hold[0] = 0.0;
    rxns.push_back(row_hold); // u237
    row_hold[0] = 1E-5;
    row_hold[1] = 1E-6;
    rxns.push_back(row_hold); // u238 n,gamma and n,2n
    row_hold[0] = 0;
    row_hold[1] = 0;
    rxns.push_back(row_hold); // u239
    row_hold[1] = 1E-6;
    rxns.push_back(row_hold); // np236 n,2n
    row_hold[0] = 1E-5;
    row_hold[1] = 1E-6;
    rxns.push_back(row_hold); // np237 n,gamma and n,2n
    row_hold[0] = 0;
    row_hold[1] = 0;
    rxns.push_back(row_hold); // np238 
    rxns.push_back(row_hold); // np239
    rxns.push_back(row_hold); // pu236
    row_hold[0] = 1E-5;
    rxns.push_back(row_hold); // pu238 n,gamma
    rxns.push_back(row_hold); // pu239 n,gamma
    rxns.push_back(row_hold); // pu240 n,gamma
    rxns.push_back(row_hold); // pu241 n,gamma
    rxns.push_back(row_hold); // pu242 n,gamma
    row_hold[0] = 0;
    rxns.push_back(row_hold); // pu243
    row_hold[0] = 1E-5;
    rxns.push_back(row_hold); // am241 n,gamma
    rxns.push_back(row_hold); // am242 n,gamma
    rxns.push_back(row_hold); // am243 n,gamma
    row_hold[0] = 0;
    rxns.push_back(row_hold); // am244
    row_hold[0] = 1E-5;
    rxns.push_back(row_hold); // cm242 n,gamma
    rxns.push_back(row_hold); // cm243 n,gamma
    row_hold[0] = 0;
    rxns.push_back(row_hold); // cm244
    
    
    time.push_back(0);
    time.push_back(0.1*8.64e+5);
    time.push_back(0.3*8.64e+5);
    time.push_back(0.5*8.64e+5);
    time.push_back(0.7*8.64e+5);
    time.push_back(8.64e+5);
//    time.push_back(1.5*8.64e+6);
//    time.push_back(2*8.64e+6);
//    time.push_back(2.5*8.64e+6);
//    time.push_back(3*8.64e+6);
//    time.push_back(3.5*8.64e+6);
//    time.push_back(4*8.64e+6);
//    time.push_back(4.5*8.64e+6);
//    time.push_back(43200000);

    
    
    
    vector<int> original_order;
    for(int i=0; i<dummy.size(); i++){
        original_order.push_back(dummy[i]);
    }
    
    vector<double> Avg_FE;
    // average fission energy in eV
    for(int i = 0; i<dummy.size(); i++){
        Avg_FE.push_back(1);
    }

    // push original conc vector BEFORE it gets sorted in library
    vector< vector<double> > concentrations_total;
    concentrations_total.push_back(conc_i);
    
    // create pointer to object
    Library_builder* test = new Library_builder;
    // also feed in average fission energy
    test->read_library_files(dummy, conc_i,  Avg_FE);
//    cout << "check order of nuclides " << endl;
//    for(int i=0; i < dummy.size(); i++){
//        cout << "nuclide[" << i+1 << "] = " << dummy[i] << endl;
//        cout << "Conc_i = " << conc_i[i] << endl;
//    }
    
//    cout << "CHECK HALFLIFE OF NUMBER 2!!!!!!!!!!!!!!" << endl;
//    cout << "lambda of " << test->get_half_life(2) << endl;

    vector<double> conc_i_unstable, conc_i_stable;
    vector<double> final_conc;

    vector<int> original_list_perma = original_order;
    // include initial conc
    
    bool rxns_exist = true;
    bool reduce_check;
    bool stable_solve = false;
    double stable_time;
    
    
// ----------------------------------------------------------------------------
// TIME LOOP STARTS HERE
// ----------------------------------------------------------------------------   
    
 for(int i=0; i<time.size()-1; i++){
     cout << "CONC BEFORE CALC!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
    for(int j=0; j<dummy.size(); j++){
        cout << "initial CONC[" << dummy[j] << "] = " << conc_i[j] << endl;
    }
    
   // initialize class that holds transition matrix
    // vector of zeros for rxn rates
    tran_mat*  tmat = new tran_mat;
    
    tmat->set_original_nuclide_order(original_list_perma);
    
    tmat->construct_transition_matrix(test, rxns, dummy, rxns_exist);

    // check if matrix needs to be reduced
    tmat->reduce_transition_matrix(dummy, rxns_exist, conc_i, conc_i_unstable, conc_i_stable, reduce_check);
    
    cout << "CHECK CONCENTRATIONS!" << endl;
    cout << "size of stable = " << conc_i_stable.size() << endl;
    cout << "size of unstable = " << conc_i_unstable.size() << endl;

    // print out transition matrix for analyzing in MATLAB
    tmat->print_transition_matrix(dummy, conc_i_unstable, conc_i_stable, "tran_mat.dat");
    cout << "print complete!" << endl;
     
     
    // call solver
    // initialize instance of solver
    depletion* decay1 = new depletion;
    decay1->set_library(test);

    decay1->set_initial_conc(conc_i_unstable, conc_i_stable, tmat);
    
//    decay1->set_reaction_rates(rxns);
    
    decay1->set_time_interval(time[i+1]-time[i]);
    
    //if(i == time.size()-2){
        // solve for stable isotopes
        stable_solve = true;
        stable_time = time[i+1]-time[0];
        decay1->set_stable_time_interval(stable_time);
    //}
    decay1->run(decay1, tmat,rxns_exist, stable_solve, 2, 30);


    decay1->get_new_conc(final_conc, tmat); 
    
    for(int j=0; j<dummy.size(); j++){
        cout << "concentration[" << original_order[j] << "] = " << final_conc[j] << endl;
    }
    
    // add new concentration vector at time i
    concentrations_total.push_back(final_conc);
    
    // assigned new vector conc to initial conc
    conc_i.clear();
    
    // new concentrations are returned in original
    // sort so order is correct for new concentrations and calc
    test->sort(original_order, final_conc, dummy.size());
    original_order = original_list_perma;
    
    for(int j=0; j<final_conc.size(); j++){
        conc_i.push_back(final_conc[j]);;
    }
    
    // destroy instances of each class for loop
    // matrix
    tmat->~tran_mat();
    decay1->~depletion();
    conc_i_unstable.clear();
    conc_i_stable.clear();
    final_conc.clear();
 }

    cout << "SIZE OF FINAL CONC IS " << concentrations_total[0].size() << endl;
    
    // call output printer
    output_proc* out1 = new output_proc;
    // print standard
    out1->print_all(original_list_perma, time, concentrations_total);
    // print matlab
    
    // print mathematica
    
    
    out1->~output_proc();
    
    
    // close instances of each class
    test->~Library_builder();
    
    
    return 0;
}

// ----------------------------------------------------------------------------
    
//    // initialize class that holds transition matrix
//    // vector of zeros for rxn rates
//    tran_mat*  tmat = new tran_mat;
//    tmat->set_original_nuclide_order(original_order);
//    tmat->construct_transition_matrix(test, rxns, dummy, rxns_exist);
//    // check if matrix needs to be reduced
//    tmat->reduce_transition_matrix(dummy, rxns_exist, conc_i, conc_i_unstable, conc_i_stable, reduce_check);
//    // print out transition matrix for analyzing in MATLAB
//    tmat->print_transition_matrix(dummy, conc_i_unstable, conc_i_stable, "tran_mat.dat");
//    // call solver
//    // initialize instance of solver
//    depletion* decay1 = new depletion;
//    decay1->set_library(test);
//
//    decay1->set_initial_conc(conc_i_unstable, conc_i_stable, tmat);
//
//    decay1->set_time_interval(86400);
//
//    decay1->run(decay1, tmat,rxns_exist, 2, 30);
//
//    decay1->get_new_conc(final_conc, tmat); 
//    
//    for(int j=0; j<dummy.size(); j++){
//        cout << "concentration[" << original_order[j] << "] = " << final_conc[j] << endl;
//    }

//    // Chain check #1
//    dummy.push_back(922380); // U238 (n,gamma)
//    dummy.push_back(922390); // U239 (decay)
//    dummy.push_back(932390); // Np239 (decay)
//    dummy.push_back(942390); // Pu239 (n,g)
//    dummy.push_back(942400); // Pu240 (n,g))
//    dummy.push_back(942410); // Pu241 (n,g))
//    dummy.push_back(952410); // Am241
//    dummy.push_back(942420); // Pu242
//    dummy.push_back(942430); // Pu243
//    dummy.push_back(952430); // Am243
//    dummy.push_back(952440); // Am244
//    dummy.push_back(962440); // Cm244
//    
//    conc_i.push_back(1E10);
//    conc_i.push_back(1E3);
//    
//    for(int i=conc_i.size(); i<dummy.size(); i++){
//        conc_i.push_back(0);
//    }
//    
//    vector<double> time;// = 86400;
//    time.push_back(0);
//    time.push_back(1000);
//    time.push_back(5000);
//    time.push_back(10000);
//    time.push_back(20000);
//    time.push_back(30000);
//    time.push_back(50000);
//    time.push_back(70000);
//    time.push_back(86400);
//    
//    
//    // 2d vector holding rxn_rates of all elements
//    vector< vector<double> > rxns;
//    vector<double> row_hold(7, 0.0); // 6 types of rxns
//   /* column neutron-nuclide interactions (n_i to n_j)
//    * 1. (n,gamma) (+000010)
//    * 2. (n,p)     (-010000)
//    * 3. (n,d)     (-010010)
//    * 4. (n,t)     (-010020)
//    * 5. (n,He-3)  (-020020)
//    * 6. (n,alpha) (-020030)
//    * 7. Fission
//    */
//    row_hold[0] = 1E-4;
//    rxns.push_back(row_hold); //  U238
//    row_hold[0] = 0;
//    rxns.push_back(row_hold); //  U239
//    row_hold[0] = 0;
//    rxns.push_back(row_hold); //  Np239
//    row_hold[0] = 1E-4;
//    rxns.push_back(row_hold); //  Pu239
//    row_hold[0] = 1E-4;
//    rxns.push_back(row_hold); //  Pu240
//    row_hold[0] = 1E-4;
//    rxns.push_back(row_hold); //  Pu241
//    row_hold[0] = 0;
//    rxns.push_back(row_hold); //  Am241
//    row_hold[0] = 1E-4;
//    rxns.push_back(row_hold); //  Pu242
//    row_hold[0] = 0;
//    rxns.push_back(row_hold); //  Pu243
//    row_hold[0] = 1E-4;
//    rxns.push_back(row_hold); //  Am243
//    row_hold[0] = 0;
//    rxns.push_back(row_hold); //  Am244
//    row_hold[0] = 0;
//    rxns.push_back(row_hold); //  Cm244


// --------------------------------------------------------------------------
//    for(int i = 1; i<6; i++){
//        row_hold.push_back(0.0);
//    }
//
//    row_hold[0] = 0.0;
//    for(int i=1; i<dummy.size(); i++){
//        rxns.push_back(row_hold);
//    }

    
    // TEST CHAIN --> U235 to Np237 with n,gamma
//    dummy.push_back(922350);
//    dummy.push_back(922360);
//    dummy.push_back(922370);
//    dummy.push_back(932370);
//
//    conc_i.push_back(1E12);
//    conc_i.push_back(1E2);
//    conc_i.push_back(1E2);
//    conc_i.push_back(1E2);
//    
//    vector< vector<double> > rxns;
//    vector<double> row_hold(7, 0.0);    
//    row_hold[0] = 1E-4;
////    for(int i = 1; i<6; i++){
////        row_hold.push_back(0.0);
////    }
//    rxns.push_back(row_hold);
//    //row_hold[0] = 0.0;
//    for(int i=1; i<dummy.size()-2; i++){
//        rxns.push_back(row_hold);
//    }
//    row_hold[0] = 0.0;
//    rxns.push_back(row_hold);
//    rxns.push_back(row_hold);
//    
//    double time = 86400;

// // FISSION CHECK
//    // Following U238, production to Pu239 and Pu240, as well as Xe135 prod
//    
//    dummy.push_back(922380); // U238
//    dummy.push_back(922390); // U239
//    dummy.push_back(932390); // Np239
//    dummy.push_back(942390); // Pu239
//    dummy.push_back(942400); // Pu240
//    dummy.push_back(531350); // I135
//    dummy.push_back(541350); // Xe135
//    dummy.push_back(551350); // Cs135
//      
//    conc_i.push_back(1E12); // U238
//    conc_i.push_back(0); // U239
//    conc_i.push_back(0); // Np239
//    conc_i.push_back(0); // Pu239
//    conc_i.push_back(0); // Pu240
//    conc_i.push_back(0); // I135
//    conc_i.push_back(0); // Xe135
//    conc_i.push_back(0); // Cs135
//    
//    // 2d vector holding rxn_rates of all elements
//    vector< vector<double> > rxns;
//    vector<double> row_hold(7, 0.0); // 6 types of rxns
//   /* column neutron-nuclide interactions (n_i to n_j)
//    * 1. (n,gamma) (+000010)
//    * 2. (n,p)     (-010000)
//    * 3. (n,d)     (-010010)
//    * 4. (n,t)     (-010020)
//    * 5. (n,He-3)  (-020020)
//    * 6. (n,alpha) (-020030)
//    * 7. Fission
//    */
//    row_hold[0] = 1E-4;
//    row_hold[6] = 1E-5;
//    rxns.push_back(row_hold); // U238 (n,gamma) + fission
//    row_hold[0] = 0;
//    row_hold[6] = 0;
//    rxns.push_back(row_hold); // U239
//    rxns.push_back(row_hold); // Np239
//    row_hold[0] = 1E-5;
//    rxns.push_back(row_hold); // Pu239(n,gamma)
//    row_hold[0] = 0;
//    rxns.push_back(row_hold); // Pu240 
//    rxns.push_back(row_hold); // I135
//    rxns.push_back(row_hold); // Xe135
//    rxns.push_back(row_hold); // Cs135
//    
//    
//    time.push_back(0);
//    time.push_back(1E1);
//    time.push_back(1E2);
//    time.push_back(1E3);
//    time.push_back(1E4);
//    time.push_back(1E5);


//    dummy.push_back(822110); // Pb211
//    dummy.push_back(832110); // Bi211
//    dummy.push_back(812070); // Tl207
//    dummy.push_back(822070); // Pb207
//    
//    conc_i.push_back(1*1E10);
//    conc_i.push_back(1*1E4);
//    conc_i.push_back(1*1E1);
//    conc_i.push_back(0.0);
// 
//// 2d vector holding rxn_rates of all elements
//    vector< vector<double> > rxns;
//    vector<double> row_hold(7, 0.0); // 6 types of rxns
//   /* column neutron-nuclide interactions (n_i to n_j)
//    * 1. (n,gamma) (+000010)
//    * 2. (n,p)     (-010000)
//    * 3. (n,d)     (-010010)
//    * 4. (n,t)     (-010020)
//    * 5. (n,He-3)  (-020020)
//    * 6. (n,alpha) (-020030)
//    * 7. Fission
//    */
//
//    rxns.push_back(row_hold); // 
//    rxns.push_back(row_hold); // 
//    rxns.push_back(row_hold); // 
//    rxns.push_back(row_hold); // )
//
//    time.push_back(0);
//    time.push_back(2000);
//    time.push_back(4000);
//    time.push_back(6000);
//    time.push_back(8000);
//    time.push_back(1E4);