/* 
 * File:   output_proc.cpp
 * Author: Daniel
 * 
 * Created on February 11, 2016, 1:02 PM
 */

#include "output_proc.h"

// constructor
output_proc::output_proc() {
}

// destructor
output_proc::~output_proc() {
}


/*
 * This function prints all the concentrations for each time step for ever nuclide
 * Output file = "final_concentrations.out"
 * 
 */
void output_proc::print_all(vector<int> nuclides, vector<double> time, vector< vector<double> > concentration){
    
    
    
    ofstream outfile("final_concentrations.out");
    
    // format is as follows
    /*              Time
     * Isotope      t = 1       t = 2       t = 3 ....
     * Nuc1
     * Nuc2
     * ...
     * 
     */
    
    // limit output line width to 80 characters
    
    outfile << "APIDA - API for Depletion Analysis" << endl;
    outfile << "Author: Daniel Lago, 2016" << endl;
    outfile << "Final Output File" << endl;
    outfile << " " << endl;
    
    
    outfile << " Time(seconds)           " ;
    for(int i=0; i< time.size(); i++){
       outfile << time[i] << "                      " ;
    }
    outfile << "\n" ;
     
    outfile << " Isotope  " << endl;
    for(int i=0; i < nuclides.size(); i++){
        outfile << " " << nuclides[i] << "   ";
        for(int j=0; j< time.size(); j++){
            outfile << "    " << scientific << setprecision(15) << concentration[j][i] << "   ";
        }
        outfile << "\n";
    }   
}


